const LIB = "tgv.js";

export default {
	input: 'build/index.js',
	// sourceMap: true,
	output: [
		{
			format: 'iife',
			file:   'dist/'+LIB,
			indent: '\t',
            name:   'tgv'
		}
	]
};

