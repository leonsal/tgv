import * as view from "../view/index.js";
import {Label} from "../label/index.js";

export const OnEnter = "OnEnter";

export type Type = "text" | "password" | "email";

export class Text extends view.View {

    protected _label: Label | null;
    protected _text: view.View;

    constructor(value: string="", type: Type="text") {

        super("div");
        this._label = null;
        this._text = new view.View("input");
        this._text.el.setAttribute("type", type);
        this._text.el.setAttribute("id", this._text.globalId());
        this._inputText().value = value;
        this.appendChild(this._text);
        this._text.el.addEventListener(view.KeyPress, (ev) =>{
            if (ev.key == "Enter") {
                this._el.dispatchEvent(new CustomEvent(OnEnter));
            }
        });
    }

    setPlaceholder(value: string) {

        this._text.el.setAttribute("placeholder", value);
    }

    setLabel(text: string) {

        if (!this._label) {
            this._label = new Label(text);
            this.insertChild(this._label, 0);
            this._label.el.setAttribute("for", this._text.globalId());
        } else {
            this._label.setText(text);
        }
    }

    setValue(value: string) {

        this._inputText().value = value;
    }

    setWidth(width: string) {

        return this._inputText().style.width = width;
    }

    value(): string {

        return this._inputText().value; 
    }

    protected _inputText(): HTMLInputElement {

        return <HTMLInputElement>this._text.el
    }
}