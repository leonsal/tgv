import * as css from "./css.js";
export const text = `
/* Tuner */
.${css.Class} {
    border: 2px solid lightgray;
    padding: 4px;
    color: black;
}
.${css.Class} span {
    font-size: 48px;
}
.${css.ClassDigitNormal} {
    color: black;
}
.${css.ClassDigitDimmed} {
    color: lightgray;
}
`