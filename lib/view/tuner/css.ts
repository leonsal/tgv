import * as base from "../cssbase/css.js";

export const Class = base.Class + "_tuner";
export const ClassDigitNormal = Class + "_digit_normal";
export const ClassDigitDimmed = Class + "_digit_dimmed";