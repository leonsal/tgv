import * as css from "./css.js";
import * as view from "../view/index.js";


export class Tuner extends view.View {

    protected _ndigits: number;
    protected _unit: string;
    protected _sep: string;
    protected _value: number;
    protected _minValue: number;
    protected _maxValue: number;

    constructor(ndigits: number, unit: string, value = 0) {

        super("div");
        this._ndigits = ndigits;
        this._unit = unit;
        this._sep = ".";
        this._value = -1;
        this._minValue = 0;
        this._maxValue = Math.pow(10, this._ndigits) - 1;

        // Style element
        this._el.className = css.Class;

        for (let di = 0; di < this._ndigits; di++) {

            // Creates digit element
            const span = new view.View("span");
            span.el.tabIndex = di + 1;
            span.el.className = css.ClassDigitNormal;
            span.el.addEventListener(view.KeyDown, (ev) => this._onKey(ev, di));
            span.el.addEventListener("wheel", (ev) => this._onWheel(ev, di));
            span.el.innerHTML = "0";
            this.appendChild(span);

            // Creates group separator element
            const rest = this._ndigits - 1 - di;
            if (rest > 0 && (rest % 3 == 0)) {
                const span = new view.View("span");
                span.el.innerHTML = this._sep;
                this.appendChild(span);
            }
        }
        // Creates span element with unit name
        const span = new view.View("span");
        span.el.innerHTML = unit;
        this.appendChild(span);

        // Sets the tuner initial value
        this.setValue(value, false);
    }

    element() {

        return this._el;
    }

    setValue(value: number, dispatch = true) {

        if (value < this._minValue || value > this._maxValue) {
            return;
        }
        if (value == this._value) {
            return;
        }
        const digits = this._value2digits(value);
        let di: number = 0;
        let dim = true;
        for (const child of this.children()) {
            if (child.el.innerHTML !== this._sep) {
                const dv = digits[di].toString();
                child.el.innerHTML = dv;
                if (dv != "0") {
                    dim = false;
                }
                di++;
                if (di >= this._ndigits) {
                    break;
                }
            }
            if (dim) {
                child.el.className = css.ClassDigitDimmed;
            } else {
                child.el.className = css.ClassDigitNormal;
            }
        }
        this._value = value;
        if (dispatch) {
            this.dispatchEvent(new CustomEvent(view.Change, {detail: {value: this._value}}));
        }
    }

    _onKey(ev: KeyboardEvent, di: number) {

        if (ev.key >= "0" && ev.key <= "9") {
            const dv = parseInt(ev.key);
            this._setDigit(di, dv);
            this._nextDigit(di)
            return
        }
        switch (ev.key) {
            case "ArrowUp":
                this._incDigit(di);
                break;
            case "ArrowDown":
                this._decDigit(di);
                break;
            case "ArrowRight":
                this._nextDigit(di);
                break;
            case "ArrowLeft":
                this._prevDigit(di);
                break;
            case "Backspace":
                this._setDigit(di, 0)
                this._prevDigit(di);
                break;
        }
    }

    _onWheel(ev: WheelEvent, di: number) {

        if (ev.deltaY < 0) {
            this._decDigit(di)
        } else {
            this._incDigit(di);
        }
    }

    protected _decDigit(di: number) {

        const pten = this._ndigits - di - 1
        const value = this._value - Math.pow(10, pten);
        this.setValue(value, true)
    }


    // Increments digit value
    protected _incDigit(di: number) {

        const pten = this._ndigits - di - 1
        const value = this._value + Math.pow(10, pten);
        this.setValue(value, true)
    }

    // Sets key focus to next digit if possible
    protected _nextDigit(di: number) {

        if (di >= this._ndigits - 1) {
            return;
        }
        di++;
        const child = this._digit(di)
        if (child != null) {
            child.el.focus();
        }
    }

    // Sets key focus to previous digit if possible
    protected _prevDigit(di: number) {

        if (di <= 0) {
            return;
        }
        di--
        const child = this._digit(di)
        if (child != null) {
            child.el.focus();
        }
    }

    // Sets the value of the specified digit
    protected _setDigit(di: number, vset: number) {

        const digits = this._value2digits(this._value);
        digits[di] = vset;
        let value = 0;
        for (let i = 0; i < this._ndigits; i++) {
            const pten = Math.pow(10, this._ndigits - i - 1);
            value += digits[i] * pten;
        }
        this.setValue(value, true);
    }

    _value2digits(value: number) {

        const digits: number[] = [];
        for (let i = 0; i < this._ndigits; i++) {
            const pten = Math.pow(10, this._ndigits - i - 1);
            const dv = Math.floor(value / pten);
            value = value % pten;
            digits.push(dv);
        }
        return digits;
    }

    _digit(di: number): view.View | null {

        let count = 0;
        for (const child of this.children()) {
            if (child.el.innerHTML === this._sep) {
                continue;
            }
            if (count == di) {
                return child;
            }
            count++;
        }
        return null;
    }
}