import * as vbuild from "../view/builder.js";
import * as view from "../view/index.js";
import * as select from "./index.js";

export interface SelectSpec extends vbuild.BaseSpec {
    view: "select",
    label?: string,
    items?: OptionSpec[],
    onChange?: (ev: Event) => void,
}

export interface OptionSpec extends vbuild.BaseSpec {
    view?: "option",
    text: string,
    value?: string,
    selected?: boolean,
    disabled?: boolean,
}


export function build(ss: SelectSpec): select.Select {

    const options: select.Option[] = [];

    if (ss.items != undefined) {
        for (const i of ss.items) {
            const value = i.value == undefined ? i.text : i.value;
            const op = new select.Option(i.text, value);
            if (i.selected) {
                op.setSelected(i.selected);
            }
            if (i.disabled) {
                op.setDisabled(i.disabled);
            }
            options.push(op)
        }
    }
    const s = new select.Select(options);
    if (ss.label != undefined) {
        s.setLabel(ss.label);
    }
    if (ss.onChange) {
        s.listen(view.Change, ss.onChange);
    }
    return s;
}


