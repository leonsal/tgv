import * as css from "./css.js";
import * as view from "../view/index.js";
import {Label} from "../label/index.js";


export class Select extends view.View {

    protected _label: Label | null;
    protected _select: view.View;

    constructor(options: Option[]=[]) {

        super("div");
        this._label = null;
        this._select = new view.View("select");
        this._select.el.setAttribute("id", this._select.globalId());
        this.appendChild(this._select);
        for (const op of options) {
            this._select.appendChild(op);
        }
    }

    setLabel(text: string) {

        if (!this._label) {
            this._label = new Label(text);
            this.insertChild(this._label, 0);
            this._label.el.setAttribute("for", this._select.globalId());
        } else {
            this._label.setText(text);
        }
    }

    add(text: string, value: string): void {

        const op = new Option(text, value);
        this._select.appendChild(op);
    }

    addOption(op: Option): void {

        this._select.appendChild(op);
    }

    removeOption(op: Option): Option|null {

        const found = this._select.removeChild(op);
        if (!found) {
            return found;
        }
        return <Option>found;
    }

    setSelected(value: string) {

        const op = this.find(value);
        if (op) {
            op.setSelected(true);
        }
    }

    setDisabled(value: string, disabled: boolean) {

        const op = this.find(value);
        if (op) {
            op.setDisabled(disabled);
        }
    }

    selected(): Option|null {

        for (const child of this._select.children()) {
            const op = <Option>child;
            if (op.selected()) {
                return op;
            }
        }
        return null;
    }

    find(value: string): Option|null {

        for (const child of this._select.children()) {
            const op = <Option>child;
            if (op.value() == value) {
                return op;
            }
        }
        return null;
    }
}

export class Option extends view.View {

    constructor(text: string, value: string) {

        super("option");
        this.el.innerHTML = text;
        this.el.value = value;
        this.el.className = css.ClassOption;
    }

    get el(): HTMLOptionElement {
        return <HTMLOptionElement>this._el;
    }

    text(): string {

        return this.el.text;
    }

    value(): string {

        return this.el.value;
    }

    disabled(): boolean {

        return this.el.disabled;
    }

    selected(): boolean {

        return this.el.selected;
    }

    setSelected(sel: boolean): void {

        this.el.selected = sel;
    }

    setDisabled(disable: boolean): void {

        this.el.disabled = disable;
    }
}
