import * as view from "../view/index.js";
import * as css from "./css.js";

export class Modal extends view.View {

    _view: view.View;
    constructor(view: view.View) {

        super("div");
        this._view = view;
        this._el.className = css.Class;
        this.appendChild(this._view);
    }

    view() {
        return this._view;
    }
}