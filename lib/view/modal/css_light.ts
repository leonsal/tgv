import * as css from "./css.js";
export const text = `
/* Modal cover */
.${css.Class} {
    display: block;
    position: absolute;
    z-index: 1;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgba(0,0,0,0.1);
}
`