import * as base from "../cssbase/css.js";

export const Class = base.Class + "_diag";
export const ClassContent = Class + "_content";
export const ClassContentTop = ClassContent + "_top";
export const ClassContentBottom = ClassContent + "_bottom";
export const ClassButtonGap = Class + "_button_gap";
