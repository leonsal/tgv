import * as view from "../view/index.js";
import * as flex from "../flex/index.js";
import * as window from "../window/index.js";
import * as css from "./css.js";


/** Diag constructor options */
export type Options = {
    top?:       view.View;      /** Optional top content view */
    title?:     string;         /** Optional title */
    resize?:    window.Resize;  /** Optional resize sizes (default: none) */
    movable?:   boolean;        /** Optional movable (default: false) */
}

/**
 * Diag extends Window and sets its content to a flexbox column layout with two areas:
 * The top area is used to display the message box main content.
 * The bottom area is a flexbox row layout and usually contains
 * the message box buttons.
 * This class is normally extended by other classes.
 */
export class Diag extends window.Window {

    protected _options: Options;
    protected _content: flex.Flex;
    protected _top: view.View;
    protected _bottom: flex.Flex;

    constructor(options: Options) {

        // The window content is a flex column layout
        const content = new flex.Flex("column");
        content.el.className = css.ClassContent;

        // Calls the Windows constructor
        super({
            title:      options.title,
            resize:     options.resize,
            movable:    options.movable,
            content:    content,
        });
        this.el.classList.add(window.Class);
        this.el.classList.add(css.Class);

        this._options = options;    // CLONE ??
        this._content = content;

        // Content top area
        this._top = new view.View("div");
        this._top.el.className = css.ClassContentTop; 
        if (options.top) {
            this._top.appendChild(options.top);
        }
        this._content.appendChild(this._top);

        // Content bottom area
        this._bottom = new flex.Flex("row")
        this._bottom.el.className = css.ClassContentBottom; 
        this._content.appendChild(this._bottom);
    }

    /**
     * Returns this message box top content view
     */
    contentTop(): view.View {

        return this._top;
    }

    /**
     * Returns this message box bottom content view (flexbox row layout)
     * Usually buttons are appended to this view.
     * The alignment of the group of buttons is set by CSS or
     * can be changed by using the method "setJustifyContent()".
     */
    contentBottom(): view.View {

        return this._bottom;
    }

    /**
     * Appends a view (usually a Button) to the bottom area of this message box
     * inserting a gap (css class:tuix_button_gap) to separate the views
     * @param v A
     */
    appendBottom(v: view.View) {

        if (this.children().length > 0) {
            this._bottom.appendChild(new view.View("div", css.ClassButtonGap));
        }
        this._bottom.appendChild(v);
    }
}
