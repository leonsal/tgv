import * as css from "./css.js";
export const text = `
/* Diag derived from Window */
.${css.Class} {
    background-color: whitesmoke;
  }
.${css.ClassContent} {
    flex-grow: 1;
    padding: 4px;
}
.${css.ClassContentTop} {
    flex-grow: 10;
}
.${css.ClassContentBottom} {
    flex-grow: 0;
    padding: 4px;
    border-top: 1px solid gray;
    justify-content: flex-end; 
 }
/* Gap between buttons */
.${css.ClassButtonGap} {
    width: 10px;
}
 ` 
  