import * as view from "./index.js";

export interface BaseSpec {
    style?: Partial<CSSStyleDeclaration>
}

export interface ViewSpec extends BaseSpec {
    view: "view",
    tag: string,
}

export function build(vs: ViewSpec): view.View {

    const v = new view.View(vs.tag);
    applySpec(vs, v);
    return v;
}

export function applySpec(vs: ViewSpec, v: view.View) {

    if (vs.style != undefined) {
        //Object.keys(vs.style).map((key:string) => {
        //    v.el.style[key] = vs.style[key];
        //} )
    }
}