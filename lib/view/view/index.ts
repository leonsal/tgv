import * as css from "../cssbase/css.js";
import * as cssModal from "../modal/css.js";

/** Common native events types */
export const Change = "change";
export const Click = "click";
export const KeyDown = "keydown";
export const KeyUp = "keyup";
export const KeyPress = "keypress";
export const MouseDown = "mousedown";
export const MouseUp = "mouseup";
export const MouseMove = "mousemove";
export const Wheel = "wheel";

export class View {

    private static nextId: number = 1;

    protected _el: HTMLElement;
    private   _globaId: string;
    protected _parent: View | null;
    protected _children: View[];
    protected _name: string;
    protected _modal: View|null

    constructor(el: string, cname?: string) {

        this._el = document.createElement(el);
        this._globaId = `${css.Class}:${View.nextId}`;
        View.nextId++;
        this._parent = null;
        this._children = [];
        this._name = "";
        this._modal = null;
        if (cname) {
            this._el.className = cname;
        }
    }

    /**
     * Returns the global id of this view
     */
    globalId(): string {

        return this._globaId;
    }

    /** 
     * Appends a child view to this view
     * @param child Child view to be appended
     */
    appendChild(child: View) {

        if (child._parent === this) {
            throw new Error("child is already a child of this view");
        }
        if (child._parent !== null) {
            child._parent.removeChild(child);
        }
        this._children.push(child);
        this._el.appendChild(child.el)
        child._parent = this;
    }

    /**
     * Removes child from this view
     * @param child Child to remove
     * @return removed child or null if not found
     */
    removeChild(child: View): View|null {

        if (child._parent === null) {
            throw new Error("child has no parent");
        }
        if (child._parent != this) {
            throw new Error("child is not child of this view");
        }
        this._el.removeChild(child.el);
        const pos = this._children.indexOf(child);
        if (pos < 0) {
            return null;
        }
        this._children.splice(pos, 1);
        child._parent = null;
        return child;
    }

    /**
     * Remove this view from its parent
     */
    removeSelf() {

        if (this._parent) {
            this._parent.removeChild(this);
        }
    }

    /**
     * Insert child view at the specified position 
     * @param child 
     * @param pos 
     */
    insertChild(child: View, pos: number) {

        if (pos < 0 || pos >= this.children().length) {
            throw new Error("Invalid child insert position");
        }
        // Insert in the DOM
        this._el.insertBefore(child.el, this._el.childNodes[pos]);
        // Insert in the view children
        this._children.splice(pos, 0, child);
    }

    /**
     * Returns the first child of this view or null if empty
     * @return first child of this view or null
     */
    firstChild(): View|null {

        if (this._children.length > 0) {
            return this._children[0]
        }
        return null;
    }

    /**
     * Returns the last child of this view or null if empty
     * @return last child of this view or null
     */
    lastChild(): View|null {

        const length = this._children.length;
        if (length > 0) {
            return this._children[length-1]
        }
        return null;
    }

    /**
     * Returns the previous sibling of this view
     * @return previous sibling of this view or none
     */
    previousSibling(): View|null {

        const parent = this._parent;
        if (parent == null) {
            return null;
        }
        const pos = parent._children.indexOf(this)
        if (pos <= 0) {
            return null;
        }
        return parent._children[pos-1];
    }

    /**
     * Returns the next sibling of this view
     * @return previous sibling of this view or none
     */
    nextSibling(): View|null {

        const parent = this._parent;
        if (parent == null) {
            return null;
        }
        const pos = parent._children.indexOf(this)
        if (pos < 0 || pos >= parent._children.length-1) {
            return null;
        }
        return parent._children[pos+1];
    }

    /**
     * Returns array of this view children
     */
    children(): View[] {

        return this._children;
    }

    parent(): View|null {

        return this._parent;
    }

    /**
     * Returns if the specified child is an immediate child of this view
     * @param child Child to verify
     * @return true if is child, false otherwise
     */
    isChild(child: View): boolean {

        if (this._children.indexOf(child) >= 0) {
            return true;
        }
        return false;
    }

    /**
     * Returns the HTMLElement of this view
     */
    get el(): HTMLElement {

        return this._el
    }

    addEventListener(type: string, cb: EventListener, options?: boolean|AddEventListenerOptions|undefined) {

        this._el.addEventListener(type, cb, options);
    }

    removeEventListener(type: string, cb: EventListener, options?: boolean|EventListenerOptions|undefined) {

        this._el.removeEventListener(type, cb, options);
    }

    dispatchEvent(event: Event) {

        this._el.dispatchEvent(event);
    }

    /**
     * Listen is a shorted alias for "addEventListener" 
     * @param type event name
     * @param cb  callback 
     * @param options  options
     */
    listen(type: string, cb: EventListener, options?: boolean|AddEventListenerOptions|undefined) {

        this.addEventListener(type, cb);

    }

    unlisten(type: string, cb: EventListener, options?: boolean|EventListenerOptions|undefined) {

        this.removeEventListener(type, cb);
    }

    setPos(left: string|number, top: string|number) {

        this._el.style.left = number2pixels(left);
        this._el.style.top = number2pixels(top);
    }

    setWidth(width: string|number) {

        this._el.style.width = number2pixels(width);
    }

    setHeight(height: string|number) {

        this._el.style.height = number2pixels(height);
    }

    setSize(width: string|number, height: string|number) {

        this.setWidth(width);
        this.setHeight(height);
    }

    setMinWidth(width: string|number) {

        this._el.style.minWidth = number2pixels(width);
    }

    setMinHeight(height: string|number) {

        this._el.style.minHeight = number2pixels(height);
    }

    setMinSize(width: string|number, height: string|number) {

        this.setMinWidth(width);
        this.setMinHeight(height);
    }

    setClassname(name: string) {

        this._el.className = name;
    }

    addClassname(...name: string[]) {

        this._el.classList.add(...name);
    }

    containsClassName(name: string): boolean {

        return this._el.classList.contains(name)
    }

    removeClassname(...name: string[]) {

        this._el.classList.remove(...name);
    }

    setModal() {
        if (this._modal) {
            return;
        }
        const modal = new View("div");
        modal._el.className = cssModal.Class;
        modal.appendChild(this);
        document.body.appendChild(modal.el);
        this._modal = modal;
    }

    closeModal() {
        if (!this._modal) {
            return;
        }
        document.body.removeChild(this._modal.el);
        this._modal = null;
    }

    /**
     * Checks if the specified window client area coordinate is
     * inside this view (considering padding, border but not margin)
     * @param x window x coordinate in pixels
     * @param y window y coordinate in pixels
     */
    inside(x: number, y: number): boolean {

        const rect = this._el.getBoundingClientRect();
        if (x < rect.left) {
            return false;
        }
        if (x > rect.right) {
            return false;
        }
        if (y < rect.top) {
            return false;
        }
        if (y > rect.bottom) {
            return false;
        }
        return true;
    }
}

function number2pixels(v: string|number): string {

    if (typeof v == "number") {
        return `${v}px`;
    } else {
        return v;
    }
}
