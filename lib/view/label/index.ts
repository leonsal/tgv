import * as view from "../view/index.js";

export class Label extends view.View {

    constructor(text: string) {

        super("label");
        this._el.innerHTML = text;
    }

    setText(text: string) {
        this._el.innerHTML = text;
    }

    text(): string {
        return this._el.innerHTML;
    }
}