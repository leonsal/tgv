
export function getVar(name: string, el?: HTMLElement) {

    if (!el) {
        el = document.body;
    }
    let text = getComputedStyle(el).getPropertyValue("--" + name);
    if (text == "") {
        console.log(`Invalid value for css var: ${name}?` );
    }
    return text.trim();
}
