export const Class = "tgv";

export const VarMainBgColor = Class + "-main-bgcolor";
export const VarFontSize = Class + "-font-size";

export const ClassCenteredBoth = Class + "_centered_both";
export const ClassCenteredHorizontal = Class + "_centered_horizontal";
export const ClassCenteredVertical = Class + "_centered_vertical";
