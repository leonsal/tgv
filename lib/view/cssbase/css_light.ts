import * as css from "./css.js";
export const text = `
/* Global variables */
:root {
    --${css.VarMainBgColor}: lightgray;
    --${css.VarFontSize}: 13px;
}
 
/* Global settings */
* {
    box-sizing: border-box;
    font-family: sans-serif;
    font-size: calc(var(--${css.VarFontSize}) + 0px);
    /* padding: 0; */
    /* margin: 0; */
}

/* Center absolute div */
.${css.ClassCenteredBoth} {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%)
  }
.${css.ClassCenteredHorizontal} {
    position: absolute;
    left: 50%;
    transform: translate(-50%)
}
.${css.ClassCenteredVertical} {
    position: absolute;
    top: 50%;
    transform: translate(0%, -50%)
}
`