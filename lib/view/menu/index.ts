import * as view from "../view/index.js";
import * as css from "./css.js";
import * as icon from "../icon/index.js";
import * as label from "../label/index.js";
import * as radio from "../radio/index.js";
import * as checkbox from "../checkbox/index.js";

/** MenuItem types  */
export type ItemType = "normal" | "separator" | "submenu" | "radio" | "checkbox";

/** MenuItem options */
export interface ItemOptions {
    type?:      ItemType,               /** type of the menu item (default: normal) */
    label?:     string,                 /** label displayed (default: "") */
    icon?:      string,                 /** optional left icon name (default: no icon */
    enabled?:   boolean,                /** enabled stated (default: true) */
    visible?:   boolean,                /** visibility state (default: true) */
    checked?:   boolean,                /** checked state for types: radio|check (default: false) */
    accel?:     string,                 /** keyboard accelerator */
    subMenu?:   Menu,                   /** sub menu object */
    id?:        string,                 /** id */
    onClick?:   (mi:MenuItem)=>void,    /** click callback */
    _bar?:      boolean,                /** used internally */
}

// Event type
export const OnItemClick = "OnItemClick";

// Set of all opened popup menus
const openedMenus: Set<Menu> = new Set();

// Reference to currently installed listener for document click events
var docClickListener: EventListener | null = null; 

// Listener to global document click events
function _docClickListener(ev: Event) {

    //console.log("doc click", openedMenus.size);
    const cev = <MouseEvent>ev;
    for (const m of openedMenus) {
        if (m.inside(cev.clientX, cev.clientY)) {
            return;
        }
    }
    // Unselects all root menus
    for (const m of openedMenus) {
        m._rootMenu()._setSelection(null);
        m._rootMenu()._resetOpenSub();
    }
}

// Adds or removes the global document click listener
function _setDocClickListener() {

    if (openedMenus.size > 0) {
        if (docClickListener == null) {
            document.addEventListener("click", _docClickListener);
            docClickListener = _docClickListener
        }
        return;
    }
    // Remove listener if necessary
    if (docClickListener != null) {
        document.removeEventListener("click", _docClickListener);
        docClickListener = null;
    }
}


export class Menu extends view.View {

    protected _autoOpen: boolean;       // initial state for openSub 
    protected _bar: boolean;            // indicates if it is a menubar
    protected _popup: boolean;          // indicates it it is a popup menu
    protected _open: boolean;           // indicates if it as an opened popup menu
    protected _openSub: boolean;        // open sub menu automatically when selecting
    protected _selected: MenuItem|null; // currently selected item

    constructor(autoOpen: boolean=true, bar: boolean=false) {

        super("div");
        this._autoOpen = autoOpen;
        this._bar = bar;
        this._popup = false;
        this._open = true;
        this._selected = null;

        if (this._bar) {
            this.addClassname(css.ClassMenuBar);
            this._openSub = this._autoOpen;
        } else {
            this.addClassname(css.ClassMenu);
            this._openSub = this._autoOpen;
        }
        this._el.setAttribute("tabindex", "0");

        this._onKeydown = this._onKeydown.bind(this);
        this._onFocus = this._onFocus.bind(this);
        this.listen("keydown", this._onKeydown);
        this.listen("focus", this._onFocus);
    }

    /**
     * Creates and appends a menu item to this menu
     * @param ops menu item options
     */
    append(options: ItemOptions): MenuItem {

        options._bar = this._bar;
        const item = new MenuItem(options);
        this.appendChild(item);
        return item;
    }

    /**
     * Creates and inserts a menu item to this menu at the specified position 
     * @param pos position to insert menu
     * @param options  menu item options
     */
    insert(pos: number, options: ItemOptions): MenuItem {

        options._bar = this._bar;
        const item = new MenuItem(options);
        this.insertChild(item, pos);
        return item;
    }

    /**
     * Returns the first MenuItem found with the specified id or null if not found.
     * Looks in this menu and all its submenus recursively and consider disabled and invisible items.
     * @param id MenuItem id supplied when the menu item was appended or inserted in this menu
     */
    getItemById(id: string): MenuItem|null {

        for (const c of this.children()) {
            const mi = <MenuItem>c;
            if (mi.id() == id) {
                return mi;
            }
            const smenu = mi.subMenu();
            if (smenu != null) {
                const smi = smenu.getItemById(id);
                if (smi != null) {
                    smi;
                }
            }
        }
        return null;
    }

    /**
     * Open this popup menu at the specified window coordinates in pixels
     * @param x 
     * @param y 
     */
    open(x: number, y: number) {

        if (this.isOpen()) {
            return;
        }
        this.el.style.left = `${x}px`;
        this.el.style.top = `${y}px`;
        this.el.style.display = "block";
        openedMenus.add(this);
        _setDocClickListener();
        this._open = true;
    }

    /**
     * Returns is this is an opened popup menu
     */
    isOpen(): boolean {

        if (this._popup && this._open) {
            return true;
        }
        return false;
    }

    /**
     * Closes this popup menu
     */
    close() {

        if (!this.isOpen()) {
            return;
        }
        this.el.style.display = "none";
        this._setSelection(null);
        // Remove this subMenu from the set of opened menus and
        // set global document click listener.
        openedMenus.delete(this);
        _setDocClickListener();
        this._open = false;
    }

    _isOpenSub(): boolean {

        return this._openSub;
    }

    _setOpenSub(state: boolean) {

        this._openSub = state;
    }

    _resetOpenSub() {

        this._openSub = this._autoOpen;
    }

    // Sets this menu as a closed popup menu
    _setPopup() {

        this.addClassname(css.ClassMenuPopup);
        this._popup = true;
        this._open = false;
    }

    // Returns the root menu of the menu chain to which this menu belongs to.
    _rootMenu(): Menu {

        let root: Menu = this;
        while (true) {
            let parent = root.parent();
            if (parent == null || !(parent instanceof MenuItem)) {
                return root;
            }
            if (parent.parent() == null) {
                return root;
            }
            root = <Menu>parent.parent();
        }
    }

    protected _onKeydown(ev: Event) {

        const kev = <KeyboardEvent>ev;
        //console.log("menu", this.globalId(), "keydown:", kev.key);
        switch (kev.key) {
            case "Enter":
                if (this._selected) {
                    this._selected._activate();
                }
                break;
            case "ArrowUp":
                if (!this._bar) {
                    this._selPreviousItem();
                }
                break;
            case "ArrowDown":
                if (!this._bar) {
                    this._selNextItem();
                }
                break;
            case "ArrowLeft":
                if (!this._bar) {
                    this._selPrevBarMenu();
                } else {
                    this._selPreviousItem();
                }
                break;
            case "Escape":
                this._selPreviousMenu();
                break;
            case "ArrowRight":
                if (!this._bar) {
                    this._selNextBarMenu();
                } else {
                    this._selNextItem();
                }
                break;
            // Checks for item accelerators from the root menu
            default:
                this._rootMenu()._checkItemsAccel(kev);
        }
        // Do not propagate event to possible parent menus
        ev.stopPropagation();
    }

    // Checks all items of this menu for keyboard accelerator which matches
    // the specified keyboard event. The item found will be activated.
    _checkItemsAccel(kev: KeyboardEvent): boolean {

        let item = <MenuItem>this.firstChild();
        while (item != null) {
            if (item._isSelectable()) {
                if (item._checkAccel(kev)) {
                    return true;
                }
            }
            item = <MenuItem>item.nextSibling();
        }
        return false;
    }

    // Selects the first item of this menu, if possible
    _selFirstItem(): MenuItem|null {

        let item = <MenuItem>this.firstChild();
        while (item != null) {
            // If menu item is not selectable, try next
            if (!item._isSelectable()) {
                item = <MenuItem>item.nextSibling();
                continue;
            }
            // Unselects the current selected item and selects the previous
            this._setSelection(item);
            return item;
        }
        return null;
    }

    // Selects the last item of this menu, if possible
    protected _selLastItem(): MenuItem|null {

        let item = <MenuItem>this.lastChild();
        while (item != null) {
            // If menu item is not selectable, try next
            if (!item._isSelectable()) {
                item = <MenuItem>item.previousSibling();
                continue;
            }
            // Unselects the current selected item and selects the previous
            this._setSelection(item);
            return item;
        }
        return null;
    }

    // Selects the previous item of this menu, if possible.
    protected _selPreviousItem(): MenuItem|null {

        if (!this._selected) {
            return null;
        }
        let item = this._selected
        while (true) {
            item = <MenuItem>item.previousSibling();
            if (item == null) {
                return this._selLastItem();
            }
            // If menu item is not selectable, try previous
            if (!item._isSelectable()) {
                continue;
            }
            // Unselects the current selected item and selects the previous
            this._setSelection(item);
            return item;
        }
        return null;
    } 

    // Selects the next item of this menu, if possible.
    protected _selNextItem(): MenuItem|null {

        if (!this._selected) {
            return null;
        }
        let item = this._selected
        while (true) {
            // Get next menu item
            item = <MenuItem>item.nextSibling();
            if (item == null) {
                return this._selFirstItem();
            }
            // If menu item is not selectable, try previous
            if (!item._isSelectable()) {
                continue;
            }
            // Unselects the current selected item and selects the next
            this._setSelection(item);
            return item;
        }
        return null;
    } 

    // Selects the previous menu, if possible
    protected _selPreviousMenu(): Menu|null {

        if (this._parent && this._parent instanceof MenuItem) {
            const menuItem = <MenuItem>this._parent;
            const menu = <Menu>menuItem.parent();
            if (menu) {
                this.el.style.display = "none";
                this._setSelection(null);
                menu.el.focus();
            }
            return menu;
        }
        return null;
    }

    // Selects the next menu associated with the currently selected item, if possible.
    protected _selNextMenu(): Menu|null {

        if (!this._selected) {
            return null;
        }
        // Checks if the currently selected item is a submenu
        const subMenu = this._selected.subMenu();
        if (!subMenu) {
            return null;
        }

        const newItem = subMenu._selFirstItem();
        if (newItem) {
            subMenu.el.focus();
            return subMenu;
        }
        // Sub menu does not contain selectable item
        return null;
    }

    protected _selPrevBarMenu() {

        const prev = this._selPreviousMenu();
        if (prev == null) {
            return;
        }
        if (prev._bar) {
            prev._selPreviousItem();
        }
    }

    protected _selNextBarMenu() {

        // Try to select next menu from this one
        // If found, done.
        const next = this._selNextMenu();
        if (next) {
            return;
        }
        // Otherwise try to select next item of possible root menu bar.
        const root = this._rootMenu();
        if (root._bar) {
            root._selNextItem()
        }
    }

    // Unselects the previous selected item if any and sets the specified item
    // of this menu as selected.
    _setSelection(item: MenuItem|null) {

        if (this._selected) {
            this._selected._removeSelection();
        }
        this._selected = item;
        if (item) {
            item._showSelection();
        }
    }

    // Called when the menu receives focus
    protected _onFocus(ev: Event) {

        // If no item selected, selects the first.
        if (!this._selected) {
            this._selFirstItem();
        }
    }
}

export class MenuBar extends Menu {

    constructor(autoOpen: boolean=false) {
        super(autoOpen, true);
    }
}

// List of valid accelerator key words.
const accelWords = [
    "ArrowDown", "ArrowLeft", "ArrowRight", "ArrowUp", "Backspace", "Delete", "End", "Enter", "Escape", "Home",
    "Insert", "Pageup", "PageDown", "Tab",
    "F0","F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12",
]
// String with valid accelerator key chars
const accelChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ~!@#$%^&*()_+=";

export class MenuItem extends view.View {

    protected _type: ItemType;
    protected _bar: boolean;
    protected _icon: icon.Icon|null;
    protected _label: label.Label|null
    protected _subMenu: Menu|undefined;
    protected _click: null|((mi: MenuItem)=>void);
    protected _id: string;
    protected _enabled: boolean;
    protected _checked: boolean;          
    protected _visible: boolean;
    protected _accel: {alt: boolean, ctrl:boolean, shift: boolean, key: string}|null;

    constructor(opts: ItemOptions) {

        super("div");
        this._type = opts.type || "normal";
        this._bar = opts._bar === undefined ? false : opts._bar;
        this._icon = null;
        this._subMenu = opts.subMenu;
        this._label = null;
        this._click = opts.onClick === undefined ? null : opts.onClick;
        this._id = opts.id === undefined ? "" : opts.id;
        this._enabled = opts.enabled === undefined ? true : opts.enabled;
        this._visible = opts.visible === undefined ? true : opts.visible;
        this._checked = opts.checked === undefined ? false : opts.checked;;
        this._accel = null;

        // Separator item
        if (this._type === "separator") {
            if (this._bar) {
                this.addClassname(css.ClassMenuBarItemSep);
                this.el.innerHTML = "|";
            } else {
                this.addClassname(css.ClassMenuItemSep);
            }
            return;
        }

        // Parses optional accelerator
        if (opts.accel !== undefined) {
            this._parseAccel(opts.accel);
        }

        // Set the class of this menu item acordingly to parent menu orientation
        if (this._bar) {
            this.addClassname(css.ClassMenuBarItem);
        } else {
            this.addClassname(css.ClassMenuItem);
        }
        //this._el.setAttribute("tabindex", "-1");

        // Left icon
        this._icon = null;
        if (!this._bar) {
            if (this._type == "radio" || this._type == "checkbox") {
                this._icon = new icon.Icon(" ");
                this._setCheck(this._checked);
            } else
            if (opts.icon) {
                this._icon = new icon.Icon(opts.icon);
            } else {
                this._icon = new icon.Icon("face");
                this._icon.el.style.color = "transparent";
            }
            this._icon.addClassname(css.ClassMenuItemIcon);
            this.appendChild(this._icon);
        }

        // Creates text label
        let text = "";
        if (opts.label) {
            text = opts.label;
        }
        this._label = new label.Label(text);
        this._label.addClassname(css.ClassMenuItemLabel);
        this.appendChild(this._label);

        // Checks if this item selects a submenu
        if (this._type == "submenu" || this._subMenu) {
            // If parent is not a MenuBar appends a submenu icon
            if (!this._bar)  {
                const ic = new icon.Icon("play_arrow");
                ic.addClassname(css.ClassMenuItemMenuIcon);
                this.appendChild(ic);
            }
            // Creates empty submenu
            if (!this._subMenu) {
                this._subMenu = new Menu();
            }
            this._subMenu._setPopup();
            this.appendChild(this._subMenu);
        } else {
            const accel = this._formatAccel();
            if (accel) {
                const acLabel = new label.Label(accel);
                acLabel.addClassname(css.ClassMenuItemAccel);
                this.appendChild(acLabel);
            }
        }

        this.setEnabled(this._enabled);
        this.setVisible(this._visible);

        this._onMouseEnter = this._onMouseEnter.bind(this);
        this._onClick = this._onClick.bind(this);
        this.listen("mouseenter", this._onMouseEnter);
        this.listen("click", this._onClick);
    }

    /**
     * Returns the text of this menu item label
     */
    text(): string|null {

        if (this._label != null) {
            return this._label.text();
        }
        return null;
    }

    /**
     * Returns if this menu item is enabled
     */
    enabled(): boolean {

        return this._enabled;
    }

    /**
     * Sets this menu item enabled state
     * @param state 
     */
    setEnabled(state: boolean) {

        this._enabled = state;
        if (!this._enabled) {
            this.el.classList.add(css.ClassMenuItemDisabled);
        } else {
            this.el.classList.remove(css.ClassMenuItemDisabled);
        }
    }

    /**
     * Sets this menu item visibility state 
     * @param state 
     */
    setVisible(state: boolean) {

        if (state) {
            if (this._bar) {
                this.el.style.display = "inline";
            } else {
                this.el.style.display = "block";
            }
            this._visible = true;
        } else {
            this.el.style.display = "none";
            this._visible = false;
        }
    }

    /**
     * Returns this menu item visibility state
     */
    visible(): boolean {

        return this._visible;
    }

    /**
     * Sets the checked state for checkbox and radio menu items 
     * Ignored if this menu item is not any these types.
     * @param state
     */
    setChecked(state: boolean) {

        if (this._type == "checkbox") {
            this._setCheck(state);
            return;
        }
        if (this._type == "radio") {
            this._setCheck(state);
            this._uncheckRadioGroup();
        }
    }

    /**
     * For menu items of type "checkbox" or "radio, returns if this menu item is checked.
     * Otherwise returns false.
     */
    checked() {

        if (this._type == "checkbox" || this._type == "radio") {
            return this._checked;
        }
        return false;
    }

    /**
     * Sets the id of this menu item
     * The supplied id is not checked if it is unique.
     * @param id 
     */
    setId(id: string) {

        this._id = id;
    }

    /**
     * Returns the id of this menu item
     */
    id(): string {

        return this._id;
    }

    /**
     * Sets the click handler for this menu item
     * @param ch 
     */
    setClickHandler(ch: null|((mi:MenuItem)=>void)) {

        this._click = ch;
    }

    /**
     * Returns the submenu associated with this item
     */
    subMenu(): Menu | undefined {

        return this._subMenu;
    }

    // Checks if this menu item or any of its sub menus has accelerator
    // specified by supplied keyboard event.
    // If positive, activates the menu item found.
    _checkAccel(kev: KeyboardEvent): boolean {

        // If this item has a sub menu, checks the sub menu items
        if (this._subMenu) {
            if (this._subMenu._checkItemsAccel(kev)) {
                return true;
            }
        }
        if (this._accel == null) {
            return false;
        }
        if (kev.altKey != this._accel.alt ||
            kev.ctrlKey != this._accel.ctrl ||
            kev.shiftKey != this._accel.shift) {
            return false;;
        }
        if (kev.key.toLowerCase() == this._accel.key.toLowerCase()) {
            this._activate();
            return true;
        }
        return false;
    }

    // Called when the menu item is clicked or enter pressed
    _activate() {

        // Item is a sub menu
        const menu = <Menu>this.parent();
        if (this._subMenu) {
            // Opens sub menu if necessary
            if (!this._subMenu.isOpen()) {
                this._openSubmenu();
                // Automatically open all sub menu from now on hover
                menu._setOpenSub(true);
            }
            this._subMenu._selFirstItem();
            this._subMenu.el.focus();
            return;
        }

        if (this._type == "checkbox") {
            this._checked = !this._checked;
            this.setChecked(this._checked);
        } else
        if (this._type == "radio") {
            this.setChecked(true);
        }
        // Get the root menu associated with this item
        let root = menu._rootMenu();
        if (root == null) {
            return;
        }
        // Unselects the root menu (closes all popup sub menus)
        root._setSelection(null);
        root._resetOpenSub();

        // Sends event from the root menu
        let event = new CustomEvent(OnItemClick, {detail: this});
        root.dispatchEvent(event);

        // Calls click handler for this menu if defined.
        if (this._click) {
            this._click(this);
        }
    }

    // Parses the user supplied accelerator string.
    // Throws exception if considered invalid.
    _parseAccel(accel: string) {

        const invalid = () => {
            throw new Error("Invalid MenuItem accelerator:" + accel);
        }

        const ac = {ctrl: false, alt: false, shift: false, key:""};
        const parts = accel.split("+");
        // Accelerator must have at least on key modifier and one key
        if (parts.length < 2) {
            invalid();
        }
        for (let i = 0; i < parts.length-1; i++) {
            const lcp = parts[i].toLowerCase();
            if (lcp == "ctrl") {
                ac.ctrl = true;
                continue;
            }
            if (lcp == "alt") {
                ac.alt = true;
                continue;
            }
            if (lcp == "shift") {
                ac.shift = true;
                continue;
            }
        }
        // At least one key modifier should be set (?)
        ac.key = parts[parts.length-1];
        if (!ac.ctrl && !ac.alt && !ac.shift) {
            invalid();
        }
        // Look for valid key chars
        if (ac.key.length == 1) {
            ac.key = ac.key.toUpperCase();
            if (!accelChars.includes(ac.key)) {
                invalid();
            }
            this._accel = ac;
            return;
        }
        // Look for valid key words
        let found = false;
        for (const w of accelWords) {
            if (w.toLowerCase() == ac.key.toLowerCase()) {
                ac.key = w;
                found = true;
                break;
            }
        }
        if (!found) {
            invalid();
        }
        this._accel = ac;
    }

    // Formats the key accelerator string
    _formatAccel(): string {

        const res: string[] = [];
        if (this._accel == null) {
            return "";
        }
        if (this._accel.alt) {
            res.push("Alt");
        }
        if (this._accel.ctrl) {
            res.push("Ctrl");
        }
        if (this._accel.shift) {
            res.push("Shift");
        }
        res.push(this._accel.key);
        return res.join("+");
    }

    // Unchecks all other radio menu items of the same radio group of this one.
    _uncheckRadioGroup() {

        // Previous items
        let prev: MenuItem = this;
        while (true) {
            prev = <MenuItem>prev.previousSibling();
            if (prev == null) {
                break;
            }
            if (prev._type == "separator") {
                break;
            }
            if (!prev._isSelectable()) {
                continue;
            }
            if (prev._type == "radio") {
                prev._setCheck(false);
            }
        }

        // Next items
        let next: MenuItem = this;
        while (true) {
            next = <MenuItem>next.nextSibling();
            if (next == null) {
                break;
            }
            if (next._type == "separator") {
                break;
            }
            if (!next._isSelectable()) {
                continue;
            }
            if (next._type == "radio") {
                next._setCheck(false);
            }
        }
    }

    // Sets the checked state for checkbox or radio menu item
    _setCheck(state: boolean) {

        if (this._type == "checkbox") {
            const icon = <icon.Icon>this._icon;
            if (state) {
                icon.setName(checkbox.iconOn);
            } else {
                icon.setName(checkbox.iconOff);
            }
            this._checked = state;
            return;
        }
        if (this._type == "radio") {
            const icon = <icon.Icon>this._icon;
            if (state) {
                icon.setName(radio.iconOn);
            } else {
                icon.setName(radio.iconOff);
            }
            this._checked = state;
            return;
        }
        throw new Error("MenuItem is not checkbox or radio");
    }


    // Returns if this item is selectable
    // Separators and disabled items are not selectable.
    _isSelectable(): boolean {

        if (this.text() == null || !this._enabled || !this._visible) {
            return false;
        }
        return true;
    }

    // Returns the position of this item relative to its parent menu
    _getPos(): {left:number, top:number, width:number, height:number} {

        const ri = this.el.getBoundingClientRect();
        // If the parent menu is not absolutely positioned, returns the
        // result obtained by getBoundingClientRect()
        const menu = <Menu>this._parent;
        const mpos = getComputedStyle(menu.el).getPropertyValue("position");
        if (mpos !== "absolute") {
            return {left: ri.left, top: ri.top, width:ri.width, height: ri.height};
        }

        // The parent menu is absolutely positioned:
        // Returns the position relative to the parent menu.
        const rmenu = menu.el.getBoundingClientRect();
        return {
            left: ri.left - rmenu.left,
            top: ri.top - rmenu.top - 2*menu.el.clientTop,
            width: ri.width,
            height: ri.height
        }
    }

    // Shows this item as selected
    // If item is a sub menu, possibly opens the sub menu popup.
    _showSelection() {

        this.addClassname(css.ClassMenuItemFocus);
        if (this._subMenu) {
            const menu = <Menu>this._parent;
            if (menu._isOpenSub()) {
                this._openSubmenu();
                if (menu instanceof MenuBar) {
                    this._subMenu.el.focus();
                }
            }
        }
    }

    // Remove selection from this item.
    // If item is a sub menu, closes the sub menu popup
    _removeSelection() {

        this.removeClassname(css.ClassMenuItemFocus);
        if (this._subMenu) {
            this._subMenu.close();
        }
    }

    // Open popup sub menu associated with this item
    _openSubmenu() {

        if (!this._subMenu) {
            return;
        }
        const r = this._getPos();
        if (!this._bar) {
            this._subMenu.open(r.left + r.width -4, r.top);
        } else {
            this._subMenu.open(r.left, r.top + r.height);
        }
    }

    _onMouseEnter(ev: Event) {

        const menu = <Menu>this.parent();
        if (this._isSelectable()) {
            menu._setSelection(this);
            menu.el.focus();
        }
    }

    _onClick(ev: Event) {

        this._activate();
        // Do not propagate event to parent menu and menuitems.
        ev.stopPropagation();
    }
}