import * as base from "../cssbase/css.js";

/** Menu bar */
export const ClassMenuBar = base.Class + "_hmenu";

/** Menu bar item */
export const ClassMenuBarItem = base.Class + "_hmenuitem";

/** Menu bar item separator */
export const ClassMenuBarItemSep = base.Class + "_hmenuitem_sep";


/** Vertical menu */
export const ClassMenu = base.Class + "_vmenu";

/** Vertical menu popup */
export const ClassMenuPopup = base.Class + "_menupopup";

/** Menu item */
export const ClassMenuItem = base.Class + "_vmenuitem";

/** Menu item selected */
export const ClassMenuItemFocus = base.Class + "_menuitem_focus";

/** Menu item disabled */
export const ClassMenuItemDisabled = base.Class + "_menuitem_disabled";

/** Menu separator */
export const ClassMenuItemSep = base.Class + "_menuitem_sep";

/** Menu item optional leading icon */
export const ClassMenuItemIcon = base.Class + "_menuitem_icon";

/** Menu item text label */
export const ClassMenuItemLabel = base.Class + "_menuitem_label";

/** Menu item accelerator label */
export const ClassMenuItemAccel = base.Class + "_menuitem_accel";

/** Menu item icon for sub menu */
export const ClassMenuItemMenuIcon = base.Class + "_menuitem_menuicon";
