import * as css from "./css.js";
export const text = `
.${css.ClassMenuBar} {
    padding: 4px;
    overflow: auto;
    background-color: aliceblue;
}
.${css.ClassMenuBarItem} {
    float: left;
    padding: 0px 6px 0px 6px;
}
.${css.ClassMenuBarItemSep} {
    float: left;
    border: 1px solid gray;
    width:  1px;
    height: 100%;
    margin: 0px 2px 0px 2px;
    color: transparent;
}

.${css.ClassMenu} {
    border: 1px solid gray;
    min-width: 150px;
    width: max-content; /* Check support */
    max-width: 50%;
    background-color: aliceblue;
}
.${css.ClassMenuPopup} {
    display: none;
    position: absolute;
}
.${css.ClassMenuItem} {
    padding: 4px 2px 4px 2px;
}
.${css.ClassMenuItemSep} {
    border-style: solid;
    border-width: 1px 0px 0px 0px;
    border-color: gray;
    height: 1px;
    margin: 4px 0px 4px 0px;
}
.${css.ClassMenuItemFocus} {
    background-color: lightgray;
}
.${css.ClassMenuItemDisabled} {
    color: lightgray;
}
.${css.ClassMenuItemIcon} {
    font-size: calc(var(--VarFontSize) + 2px);
    vertical-align: middle;
    padding: 0px 4px 0px 4px;
}
.${css.ClassMenuItemLabel} {
    /*padding: 4px 0px 14px 0px; */
    vertical-align: middle;
}
.${css.ClassMenuItemAccel} {
    float: right;
    color: gray;
}
.${css.ClassMenuItemMenuIcon} {
    float: right;
    font-size: calc(var(--VarFontSize) + 2px);
    vertical-align: middle;
    padding: 0px 4px 0px 4px;
}
`