import * as menu from "./index.js";

export interface MenuItemSpec extends menu.ItemOptions {
    menuSpec?: MenuSpec
}

export interface MenuBarSpec {
    view: "menubar",
    items?: MenuItemSpec[],
    onItemClick?: (ev:Event)=>void,
}

export interface MenuSpec {
    view?: "menu",
    items?: MenuItemSpec[],
    onItemClick?: (ev:Event)=>void,
}

export function build(mbs: MenuBarSpec|MenuSpec): menu.MenuBar {

    let mb: menu.MenuBar | menu.Menu;
    if (mbs.view == "menubar") {
        mb = new menu.MenuBar();
    } else {
        mb = new menu.Menu();
    }

    if (mbs.onItemClick != undefined) {
        mb.listen(menu.OnItemClick, mbs.onItemClick);
    }

    const items = mbs.items;
    if (items == undefined) {
        return mb;
    }
    for (const i of items ) {
        if (i.menuSpec != undefined) {
            const m = build(i.menuSpec);
            i.subMenu = m;
        }
        mb.append(i);
    }
    return mb;
}

