import * as view from "../view/index.js";
import {Icon} from "../icon/index.js";
import {Label} from "../label/index.js";
import * as css from "./css.js";

//export const CssClass = view.CssClass + "_btn";

export const enum Type {
    IconLeft    = "icon-left",
    IconTop     = "icon-top",
    IconRight   = "icon-right",
    IconBottom  = "icon-bottom",
}

export class Button extends view.View {

    _label: Label;
    _icon:  Icon|null;

    constructor(text: string, icon: string="", type: Type=Type.IconLeft) {

        super("button");
        this._el.className = css.Class;

        this._label = new Label(text);
        this._label.addClassname(css.ClassLabel);
        this._icon = null;
        if (icon) {
            this._icon = new Icon(icon);
            this._icon.addClassname(css.ClassIcon);
        }
        if (type == Type.IconLeft) {
            if (this._icon) {
                this.appendChild(this._icon);
            }
            this.appendChild(this._label);
            return;
        }
        if (type == Type.IconRight) {
            this.appendChild(this._label);
            if (this._icon) {
                this.appendChild(this._icon);
            }
            return;
        }
        if (type == Type.IconTop) {
            if (this._icon) {
                this.appendChild(this._icon);
                this._icon.el.style.display = "block";
            }
            this.appendChild(this._label);
            return;
        }
        if (type == Type.IconBottom) {
            this.appendChild(this._label);
            this._label.el.style.display = "block";
            if (this._icon) {
                this.appendChild(this._icon);
            }
            return;
        }
    }

    setText(text: string) {

        this._label.setText(text);
    }

    setIcon(name: string) {

        if (!this._icon) {
            return;
        }
        this._icon.setName(name);
    }

    text(): string {

        return this._label.text();
    }
}
