import * as css from "./css.js";
export const text = `
/* Button */
.${css.Class} {
    padding: 1px 4px;
}
/* Button icon */
.${css.ClassIcon} {
    font-size: 14px;
    vertical-align: middle;
    padding: 0px 4px 0px 4px;
}
/* Button label */
.${css.ClassLabel} {
    vertical-align: middle;
}
`