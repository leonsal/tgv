import {Button} from "../button/index.js";
import * as css  from "./css.js";

export const iconOff = "radio_button_unchecked";
export const iconOn  = "radio_button_checked";

export const OnSelect = "OnSelect";

export class Group extends EventTarget {

    protected _radios: Radio[];

    constructor() {

        super();
        this._radios = []
    }

    /** Returns the selected radio button on null if none is selected */
    selected(): Radio|null {

        for (const r of this._radios) {
            if (r.value()) {
                return r;
            }
        }
        return null;
    }

    listen(type: string, cb: EventListener, options?: boolean|AddEventListenerOptions|undefined) {

        this.addEventListener(type, cb);

    }

    unlisten(type: string, cb: EventListener, options?: boolean|EventListenerOptions|undefined) {

        this.removeEventListener(type, cb);
    }

    // Called by the radio button constructor
    _addRadio(r: Radio) {

        this._radios.push(r);
    }

    // Called by the radio button when its value changes to true.
    _set(r: Radio) {

        for (const other of this._radios) {
            if (other != r) {
                other.setValue(false);
            }
        }
        this.dispatchEvent(new CustomEvent(OnSelect, {detail: {group: r}}));
    }
}

export class Radio extends Button {

    protected _value: boolean;
    protected _group: Group|null;

    constructor(text: string, group: Group|undefined) {

        super(text, iconOff);
        this._el.className = css.Class;
        this._value = false;
        this._group = null;
        if (group) {
            this._group = group;
            this._group._addRadio(this);
        }
        this._onClick = this._onClick.bind(this);
        this.listen("click", this._onClick);
    }

    value() {

        return this._value;
    }

    setValue(value: boolean) {

        if (this._value == value) {
            return;
        }
        this._value = value;
        if (this._value) {
            this.setIcon(iconOn);
        } else {
            this.setIcon(iconOff);
        }
        if (this._group && this._value) {
            this._group._set(this);
        }
    }

    _onClick(ev: Event) {

        this.setValue(true);
    }
}


