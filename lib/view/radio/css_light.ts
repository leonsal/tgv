import * as css from "./css.js";
export const text = `
/* Radio button */
.${css.Class} {
  border: none;
  background-color: transparent;
}
.${css.ClassIcon} i {
  font-size: 16px;
  vertical-align: middle;
  padding: 0px 4px 0px 4px;
}
.${css.ClassLabel} label {
  vertical-align: middle;
}
`
