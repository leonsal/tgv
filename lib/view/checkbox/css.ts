import * as base from "../cssbase/css.js";

export const Class = base.Class + "_check";
export const ClassIcon = Class + "_icon";
export const ClassLabel = Class + "_label";
