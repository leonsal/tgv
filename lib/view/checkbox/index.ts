import {Button} from "../button/index.js";
import * as css from "./css.js";

export const iconOn  = "check_box";
export const iconOff = "check_box_outline_blank";

export class Checkbox extends Button {

    protected _value: boolean;

    constructor(text: string) {

        super(text, iconOff);
        this._el.className = css.Class;
        this._value = false;
        this._onClick = this._onClick.bind(this);
        this.listen("click", this._onClick);
    }

    value() {

        return this._value;
    }

    setValue(value: boolean) {

        if (this._value == value) {
            return;
        }
        this._value = value;
        if (this._value) {
            this.setIcon(iconOn);
        } else {
            this.setIcon(iconOff);
        }
    }

    _onClick(ev: Event) {

        this.setValue(!this._value);
    }
}


