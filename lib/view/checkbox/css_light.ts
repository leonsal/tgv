import * as css from "./css.js";
export const text = `
/* Checkbox */
.${css.Class} {
    border: none;
    background-color: transparent;
}
/* Checkbox icon */
.${css.ClassIcon} i {
    font-size: 16px;
    vertical-align: middle;
    padding: 0px 4px 0px 4px;
}
/* Checkbox label */
.${css.ClassLabel} label {
    vertical-align: middle;
 }
 `