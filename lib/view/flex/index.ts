import * as view from "../view/index.js";
import * as css from "./css.js";

export type Direction =
    "row" | "row-reverse" | "column" | "column-reverse";

export type Display =
    "flex" | "inline-flex";

export type Wrap =
    "wrap" | "nowrap" | "wrap-reverse";

export type AlignItems =
    "stretch" | "center" | "flex-start" | "flex-end" | "baseline";

export type JustifyContent =
    "flex-start" | "flex-end" | "center" | "space-between" | "space-around" | "space-evenly";

export type AlignContent =
    "stretch" | "flex-start" | "flex-end" | "center" | "space-between" | "space-around" | "space-evenly";

export type AlignSelf =
    "auto" | "flex-start" | "flex-end" | "center" | "baseline" | "stretch";

export type ChildProps = {
    alignSelf?:     AlignSelf,
    flexGgrow?:     string,
    flexShrink?:    string,
    flexBasis?:     string,
    order?:         number,
}

export type Gap = {
    size?:      string,        // width/height of the gap
    flexGrow?:  string,
}


export class Flex extends view.View {

    constructor(dir: Direction="row", type: Display="flex") {

        super("div");
        this.el.style.display = type;
        this.el.style.flexDirection = dir;
    }

    setWrap(w: Wrap) {

        this.el.style.flexWrap = w;
    }

    setAlignItems(a: AlignItems) {

        this.el.style.alignItems = a;
    }

    setJustifyContent(j: JustifyContent) {

        this.el.style.justifyContent = j;
    }

    setAlignContent(a: AlignContent) {

        this.el.style.alignContent = a;
    }

    appendGap(gap: Gap) {
       
        const g = this._createGap(gap);
        this.appendChild(g);
    }

    protected _createGap(gap: Gap): view.View {

        const g = new view.View("div");
        g.el.className = css.ClassGap;
        this._setGapProps(g, gap);
        return g;
    }

    protected _setGapProps(g: view.View, gap: Gap) {

        let dir = this.el.style.flexDirection;
        if (!dir) {
            dir = "row";
        }
        if (gap.size !== undefined) {
            if (dir.startsWith("row")) {
                g.el.style.width = gap.size;
            } else {
                g.el.style.height = gap.size;
            }
        }
        if (gap.flexGrow !== undefined) {
            g.el.style.flexGrow = gap.flexGrow;
        }
    }
}