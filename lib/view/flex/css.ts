import * as base from "../cssbase/css.js";

export const Class = base.Class + "_flex";
export const ClassGap = base.Class + "_flex_gap";