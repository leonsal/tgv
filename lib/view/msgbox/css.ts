import * as base from "../cssbase/css.js";

export const Class = base.Class + "_msgbox";
export const ClassContentTop = Class + "_content_top";
export const ClassContentBottom = Class + "_bottom";
export const ClassIcon = Class + "_icon";
export const ClassVarIconName = Class + "_icon_name";

export const VarInfoBoxIconName = base.Class + "_infobox_icon_name";
export const ClassInfoBoxIcon = base.Class + "_infobox_icon";

export const VarWarningBoxIconName = base.Class + "_warningbox_icon_name";
export const ClassWarningBoxIcon = base.Class + "_warningbox_icon";

export const VarConfirmBoxIconName = base.Class + "_confirmbox_icon_name";
export const ClassConfirmBoxIcon = base.Class + "_confirmbox_icon";