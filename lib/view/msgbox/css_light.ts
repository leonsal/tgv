import * as css from "./css.js";
export const text = `
/* Message box default size */
.${css.Class} {
    min-width: 300px;
    min-height: 100px;
    width: 300px;
    height: auto;
  }
/* Message box content top layout */
.${css.ClassContentTop} {
    display: flex;
    flex-direction: row;
    align-items: center;
}
.${css.ClassIcon} {
    padding: 0px 6px 2px 2px; 
    font-size: 40px;
}
.${css.ClassInfoBoxIcon} {
    color: blue;
}
.${css.ClassConfirmBoxIcon} {
    color: green;
}
.${css.ClassWarningBoxIcon} {
    color: red;
}
/* Message box global variables */
:root {
    --${css.VarInfoBoxIconName}: info;
    --${css.VarWarningBoxIconName}: warning;
    --${css.VarConfirmBoxIconName}: live_help;
}
`