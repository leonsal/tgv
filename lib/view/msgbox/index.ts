import * as diag from "../diag/index.js";
import * as button from "../button/index.js";
import * as label from "../label/index.js";
import * as icon from "../icon/index.js";
import * as css from "./css.js";
import * as cssbase from "../cssbase/index.js";
import * as window from "../window/index.js";

export type Options = {
    title: string,
    msg?: string,
    movable?: boolean,
    resize?: window.Resize,
    icon?: string,
    iconClass?: string,
    buttons?: {label: string, id: string, icon?: string}[]
    close?: boolean,
}
export type Callback = (id:string) => void;

/**
 * MsgBox is derived from Diag 
 */
export class MsgBox extends diag.Diag {

    constructor(options: Options, cb?: Callback) {

        super(options);
        this._el.classList.add(css.Class);

        // Add class name to top content
        const top = this.contentTop();
        top.el.classList.add(css.ClassContentTop);

        // Creates and adds icon 
        if (options.icon) {
            const ic = new icon.Icon(options.icon);
            ic.addClassname(css.ClassIcon);
            if (options.iconClass) {
                ic.addClassname(options.iconClass);
            }
            top.appendChild(ic);
        }

        // Creates and adds user message
        if (options.msg) {
            top.appendChild(new label.Label(options.msg));
        }

        // Creates buttons
        if (options.buttons) {
            for (const bt of options.buttons) {
                const b = new button.Button(bt.label, bt.icon);
                this.appendBottom(b);
                b.listen("click", (ev: Event) => {
                    if (cb) {
                        cb(bt.id);
                    }
                    if (options.close) {
                        this.close();
                    }
                });
            }
    }
    }
}

const InfoBoxDefaultTitle = "Information";

/**
 * Creates and inserts in the page an information modal message box
 * @param msg Message text
 * @param options  Optional configuration options
 * @param cb optional callback called when button is clicked.
 */
export function info(msg: string, options: Options|null|undefined=null, cb?: Callback): MsgBox {

    // Set default options
    const ops: Options = {
        title: InfoBoxDefaultTitle,
        msg: msg,
        movable: true,
        resize: "none",
        icon: cssbase.getVar(css.VarInfoBoxIconName),
        iconClass: css.ClassInfoBoxIcon,
        buttons: [{label: "OK", id: "ok", icon: "check"}],
        close: true,
    }
    // Merge with user options
    if (options) {
        Object.assign(ops, options);
    }
    // Creates message box 
    const mb = new MsgBox(ops, cb);
    mb.setModal();
    mb.center("both");
    return mb;
}

const WarningBoxDefaultTitle = "Warning";

export function warning(msg: string, options: Options|null|undefined=null, cb?: Callback): MsgBox {

    const ops: Options = {
        title: WarningBoxDefaultTitle,
        msg: msg,
        movable: true,
        resize: "none",
        icon: cssbase.getVar(css.VarWarningBoxIconName),
        iconClass: css.ClassWarningBoxIcon,
        buttons: [{label: "OK", id: "ok", icon: "check"}],
        close: true,
    }
    if (options) {
        Object.assign(ops, options);
    }
    const mb = new MsgBox(ops, cb);
    mb.setModal();
    mb.center("both");
    return mb;
}

const ConfirmBoxDefaultTitle = "Confirmation";

export function confirm(msg: string,  options: Options|null|undefined=null, cb?: Callback) {

    const ops: Options = {
        title: ConfirmBoxDefaultTitle,
        msg: msg,
        movable: true,
        resize: "none",
        icon: cssbase.getVar(css.VarConfirmBoxIconName),
        iconClass: css.ClassConfirmBoxIcon,
        buttons: [
            {label: "OK", id: "ok", icon: "check"},
            {label: "Cancel", id: "cancel", icon: "close"},
        ],
        close: true,
    }
    if (options) {
        Object.assign(ops, options);
    }
    const mb = new MsgBox(ops, cb);
    mb.setModal();
    mb.center("both");
    return mb;
}

export function custom(options: Options, cb?: Callback) {

    const mb = new MsgBox(options, cb);
    mb.setModal();
    mb.center("both");
    return mb;
}
