import * as css from "./css.js";
export const text = `
/* Window */
.${css.Class} {
    position: absolute;
    border: 2px solid gray;
    overflow: auto;               /* for resize to work */
}
/* Optional window header */
.${css.ClassHeader} {
    padding: 2px 4px 2px 4px;
    border-bottom: 1px solid lightgray;
    background-color: white;
    overflow: hidden;
  }
/* Window header label */
.${css.ClassHeader} label {
    vertical-align: middle;
}
/* Buttons inside the window header */
.${css.ClassHeader} button {
    vertical-align: middle; 
    float: right; 
    background-color: transparent;
    padding: 0px 0px;
    border: none;
    margin: none;
    cursor: default;
}
.${css.ClassHeader} button i {
    font-size: calc(var(--VarFontSize) + 3px);
    padding: 0px 0px 0px 2px;
}
.${css.ClassHeader} button:hover {
    color: blue;
}
`