import * as view from "../view/index.js";
import { Label } from "../label/index.js";
import { Button } from "../button/index.js";
import * as flex from "../flex/index.js";
import * as css from "./css.js";
import * as cssBase from "../cssbase/css.js";
export * from "./css.js";

export type Options = {
    title?:     string;
    resize?:    Resize;
    movable?:   boolean;
    content?:   view.View;
}

export type Resize = "both"|"horizontal"|"vertical"|"none";
export type Center = Resize;

const cursorResizeS     = "s-resize";
const cursorResizeE     = "e-resize";
const cursorResizeSE    = "se-resize";
const cursorDefault     = "default";

export class Window extends flex.Flex {

    private static _windows: Window[] = [];
    private static _resizing: Window|null = null;
    private static _nextZ: number = 1;

    protected _header: Header|null;
    protected _content: view.View|null;
    protected _resize: Resize;
    protected _movable: boolean;
    protected _dragable: string;
    protected _drag: string;
    protected _lastX: number;
    protected _lastY: number
    protected _center: boolean;

    constructor(options: Options) {

        // Window is flex box column layout
        super("column");
        this._el.className = css.Class;

        // Binds internal methods
        this._onMouseMove = this._onMouseMove.bind(this);
        this._onMouseDown = this._onMouseDown.bind(this);
        this._onMouseUp = this._onMouseUp.bind(this);
        this.addEventListener(view.MouseDown, this._onMouseDown);

        // Creates and appends header if title was set.
        this._header = null;
        if (options.title !== undefined) {
            this._header = new Header(options);
            super.appendChild(this._header);
        }

        // Sets resizable state
        this._resize = "none";
        if (options.resize !== undefined) {
            this.setResize(options.resize);
        }

        // Sets movable state
        this._movable = false;
        if (options.movable !== undefined) {
            this.setMovable(options.movable);
        }

        this._dragable = "";
        this._drag = "";
        this._lastX = 0;
        this._lastY = 0;
        this._center = false;

        // Appends optional content;
        this._content = null;
        if (options.content) {
            this.setContent(options.content);
        }

        // Sets this window z index to next index and adds window to global windows array.
        this.el.style.zIndex = Window._nextZ.toString();
        Window._nextZ++;
        Window._windows.push(this);
    }

    /**
     * Sets the content view of this window
     * @param content content view
     */
    setContent(content: view.View) {

        if (this._content) {
            this.removeChild(this._content);
        }
        this._content = content;
        this.appendChild(this._content);
    }

    /**
     * Returns the content view of this window
     * @return content view or null
     */
    content(): view.View|null {

        return this._content
    }

    /**
     * Appends icon button to the window header.
     * Only has effect if the window was created with a title
     * @param name Name of the icon
     * @return created icon button or null
     */
    appendHeaderIcon(name: string): Button|null {

        if (this._header) {
            return this._header.appendIcon(name);
        }
        return null;
    }
      
    /**
     * Center the window on inside its container.
     * @param center 
     */
    center(center: Center) {

        const width = this._el.style.width;
        const height = this._el.style.height;
        // If width and height defined
        if (width && height) {
            if (center == "both" || center == "horizontal") {
                this._el.style.left = `calc(50% - ${width}/2`;
            }
            if (center == "both" || center == "vertical") {
                this._el.style.top = `calc(50% - ${height}/2`;
            }
        } else {
            this.removeClassname(cssBase.ClassCenteredBoth, cssBase.ClassCenteredHorizontal, cssBase.ClassCenteredVertical);
            if (center == "both") {
                this.addClassname(cssBase.ClassCenteredBoth);
            }
            if (center == "horizontal") {
                this.addClassname(cssBase.ClassCenteredHorizontal);
            }
            if (center == "vertical") {
                this.addClassname(cssBase.ClassCenteredVertical);
            }
        }
    }

    close() {
        if (this._modal) {
            this.closeModal();
        } else {
            this.removeSelf();
        }
        const pos = Window._windows.indexOf(this)
        if (pos < 0) {
            return;
        }
        Window._windows.splice(pos, 1);
    }

    dispose() {
        // Remove window from static array
        // Remove from parent/dom
    }

    setMovable(state: boolean) {

        this._movable = true;
    }

    movable(): boolean {

        return this._movable;
    }

    setTitle(title: string) {

        if (this._header) {
            this._header.setTitle(title);
        }
    }

    isTop() {

        let maxZ = -1;
        let top: Window|null = null;
        for (const w of Window._windows) {
            const z = parseInt(<string>w.el.style.zIndex);
            if (z > maxZ) {
                maxZ = z;
                top = w;
            }
        }
        if (top == this) {
            return true;
        }
        return false;
    }

    /**
     * Set this window as the top window
     */
    setTop() {

        // If less than 2 windows, nothing to do.
        const windows = Window._windows;
        if (Window._windows.length < 2) {
            return;
        }

        // Sort windows by ascending z order
        windows.sort((a:Window, b:Window) => {
            const za = parseInt(<string>a.el.style.zIndex);
            const zb = parseInt(<string>b.el.style.zIndex);
            if (za > zb) {
                return 1;
            }
            if (za < zb) {
                return -1;
            }
            return 0;
        });

        // Get top window. If this window is the top, nothing to do.
        const top = windows[windows.length-1];
        if (top == this) {
            return;
        }

        // Find this window in the ordered array
        let found = -1;
        for (const [pos, w] of Window._windows.entries()) {
            if (w == this) {
                found = pos;
                break;
            }
        }
        // Shouldn't happen
        if (found < 0) {
            return;
        }

        // Sets the zIndex of this window to the previous top window
        this.el.style.zIndex = top.el.style.zIndex;
        // Decrements the zIndex of all windows after the position of this one.
        for (let i = found + 1; i < windows.length; i++) {
            const w = windows[i];
            let z = parseInt(<string>w.el.style.zIndex) - 1;
            w.el.style.zIndex = z.toString();
        }
    }

    setResize(resize: Resize) {

        if (this._resize == resize) {
            return;
        }
        // If previous resize was none, install listeners
        if (this._resize == "none") {
            this.addEventListener(view.MouseMove, this._onMouseMove);
            window.addEventListener(view.MouseUp, this._onMouseUp);
        }
        // If current resize is none, remove listeners
        if (resize == "none") {
            this.removeEventListener(view.MouseMove, this._onMouseMove);
            window.removeEventListener(view.MouseUp, this._onMouseUp);
        }
        this._resize = resize;
    }

    protected _onMouseMove(ev: Event) {

        ev.preventDefault();
        // If there is a window being resized and it is not this one, nothing to do.
        if (Window._resizing != null && Window._resizing != this) {
            return;
        }

        // Checks if this window is being dragged
        const mev = <MouseEvent>ev;
        if (this._drag != "") {
            this._onDrag(mev);
            return;
        }

        const r = this.el.getBoundingClientRect();
        const pad = 8;
        let right = false;
        let bottom = false;
        if (mev.clientX > r.left + r.width - pad) {
            right = true;
        }
        if (mev.clientY > r.top + r.height - pad) {
            bottom = true;
        }
        if (right && bottom && this._resize == "both") {
            this.el.style.cursor = cursorResizeSE;
            this._dragable = cursorResizeSE;
        } else
        if (right && (this._resize == "both" || this._resize == "horizontal")) {
            this.el.style.cursor = cursorResizeE;
            this._dragable = cursorResizeE;
        } else
        if (bottom && (this._resize == "both" || this._resize == "vertical")) {
            this.el.style.cursor = cursorResizeS;
            this._dragable = cursorResizeS;
        } else {
            this.el.style.cursor = cursorDefault;
            this._dragable = "";
        }
    }

    protected _onMouseDown(ev: Event) {

        this.setTop()
        if (this._dragable == "") {
            return;
        }

        // Starts drag
        Window._resizing = this;
        const mev = <MouseEvent>ev;
        this._drag = this._dragable;
        this._setGlobalCursor(this._drag);
        this._lastX = mev.clientX;
        this._lastY = mev.clientY;
        // Remove local mouse move listener and adds global one
        this.unlisten(view.MouseMove, this._onMouseMove);
        window.addEventListener(view.MouseMove, this._onMouseMove);
        ev.preventDefault();
    }

    protected _onMouseUp(ev: Event) {

        if (this._drag == "") {
            return;
        }

        // Stops drag
        this._drag = "";
        Window._resizing = null;
        // Remove global mouse move listener and adds local one
        window.removeEventListener(view.MouseMove, this._onMouseMove);
        this.addEventListener(view.MouseMove, this._onMouseMove);
        this._setGlobalCursor(cursorDefault);
    }

    protected _onDrag(ev: MouseEvent) {

        const deltaX = ev.clientX - this._lastX;
        const deltaY = ev.clientY - this._lastY;
        const width = this.el.offsetWidth + deltaX;
        const height = this.el.offsetHeight + deltaY;
        if ((this._resize == "both" || this._resize == "horizontal") &&
            (this._drag == cursorResizeE || this._drag == cursorResizeSE)) {
            this.el.style.width = `${width}px`;
        }
        if ((this._resize == "both" || this._resize == "vertical") &&
            (this._drag == cursorResizeS || this._drag == cursorResizeSE)) {
            this.el.style.height = `${height}px`;
        }
        this._lastX = ev.clientX;
        this._lastY = ev.clientY;
    }

    protected _setGlobalCursor(cursor: string) {

        const html = document.getElementsByTagName("html")[0];
        html.style.cursor = cursor;
    }
}


class Header extends view.View {

    protected _label: Label
    protected _lastX: number;
    protected _lastY: number

    constructor(options: Options) {

        super("div");
        this._el.className = css.ClassHeader;

        // Creates and appends label for optional title
        this._label = new Label("");
        this.appendChild(this._label);
        if (options.title) {
            this._label.setText(options.title);
        }

        this._lastX = 0;
        this._lastY = 0;
        this._onMouseDown = this._onMouseDown.bind(this);
        this._onMouseUp = this._onMouseUp.bind(this);
        this._onMouseMove = this._onMouseMove.bind(this);
        this.listen(view.MouseDown, this._onMouseDown);
    }

    setTitle(title: string) {

        this._label.setText(title);
    }

    appendIcon(name: string): Button {

        const b = new Button("", name);
        this.appendChild(b);
        return b;
    }

    protected _onMouseDown(ev: Event) {

        const win = <Window>this._parent;
        if (!win.movable()) {
            return;
        }
        const mev = <MouseEvent>ev;
        this._lastX = mev.clientX;
        this._lastY = mev.clientY;
        window.addEventListener(view.MouseMove, this._onMouseMove);
        window.addEventListener(view.MouseUp, this._onMouseUp);
    }

    protected _onMouseUp(ev: Event) {

        window.removeEventListener(view.MouseMove, this._onMouseMove);
        window.removeEventListener(view.MouseUp, this._onMouseUp);
    }

    protected _onMouseMove(ev: Event) {

        const mev = <MouseEvent>ev;
        const deltaX = mev.clientX - this._lastX;
        const deltaY = mev.clientY - this._lastY;
        const win = <Window>this._parent;
        const px = win.el.offsetLeft + deltaX;
        const py = win.el.offsetTop + deltaY;
        win.setPos(px, py);
        this._lastX = mev.clientX;
        this._lastY = mev.clientY;
        mev.preventDefault();
    }
}
