import * as base from "../cssbase/css.js";

export const Class = base.Class + "_win";
export const ClassHeader = base.Class + "_winhead";
export const ClassContent = base.Class + "_wincontent";