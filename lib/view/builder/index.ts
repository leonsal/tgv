import * as view from "../view/index.js";
import * as select from "../select/builder.js";
import * as menu from "../menu/builder.js";

type UnionSpec = select.SelectSpec | menu.MenuSpec | menu.MenuBarSpec;

export function build(vs: UnionSpec): view.View | null {

    switch (vs.view) {
        case "select":
            return select.build(vs);
        case "menubar":
        case "menu":
           return menu.build(vs);
    }
    return null;
}
