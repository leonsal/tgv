import * as css from "./css.js";
import * as view from "../view/index.js";


export class Icon extends view.View {

    constructor(name: string, klass: string="material-icons") {

        super("i");
        this._el = document.createElement("i");
        this._el.className = klass;
        this._el.classList.add(css.Class);
        this._el.innerHTML = name;
    }

    setName(name: string) {

        this._el.innerHTML = name;
    }
}