import * as view from "../view/index.js";

export type AlignItems = 
    "auto" | "normal" | "start" | "end" | "center" | "stretch" |
    "baseline" | "first baseline" | "last baseline";
export type JustifyItems = AlignItems;
export type AlignSelf = AlignItems;
export type JustifySelf = JustifyItems;

export type AlignContent = 
    "normal" | "start" | "end" | "center" | "stretch" |
    "space-around" | "space-between" | "space-evenly" |
    "baseline" | "first baseline" | "last baseline";
export type JustifyContent = AlignContent;

export type AutoFlow =
    "row" | "column" | "dense" | "row dense" | "column dense";


export class Grid extends view.View {

    constructor(template: string) {

        super("div");
        this.el.style.display = "grid";
        this.el.style.gridTemplate = template;
    }

    setGap(gap: string) {

        this.el.style.gridGap = gap;
    }
    
    setRowGap(rowgap: string) {
    
        this.el.style.rowGap = rowgap;
    }
    
    setColumnGap(colgap: string) {
    
        this.el.style.gridColumnGap = colgap;
    }
    
    setTemplate(template: string) {
    
        this.el.style.gridTemplate = template;
    }
    
    setTemplateColumns(template: string) {
    
        this.el.style.gridTemplateColumns = template;
    }
    
    setTemplateRows(template: string) {
    
        this.el.style.gridTemplateRows = template;
    }

   /**
    * Sets the "justify-content" property of this grid style
    * to justify the complete grid along the row axis when the grid is smaller
    * than its container.
    */
    setJustifyContent(j: JustifyContent) {

	    this.el.style.justifyContent = j;
    }

    setJustifyItems(j: JustifyItems) {

	    this.el.style.justifyItems = j;
    }
 
    // AlignContent sets the "align-content" property of this grid style
    // to align the complete grid along the column axis when the grid is smaller
    // than its container. The suported values are:
    // css.Start, css.End, css.Center, css.Stretch, css.SpaceAround,
    // css.SpaceBetween andcss.SpaceEvenly.
    setAlignContent(align: AlignContent) {

        this.el.style.alignContent = align;
    }

    setAlignItems(align: AlignItems) {

	    this.el.style.alignItems = align;
    }

    setAutoFlow(flow: AutoFlow) {

	    this.el.style.gridAutoFlow = flow;
    }

    setAutoRows(rows: string) {

	    this.el.style.gridAutoRows = rows;
    }
}

