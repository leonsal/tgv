import * as css from "./css.js";
export const text = `
/* Splitter horizontal */
.${css.ClassH} {
    border: none;
  }
 /* Horizontal Splitter separator */
.${css.ClassHsep} {
    float: left;
    height: 100%;
    cursor: ew-resize;
    background-color: white;
 }
 /* Splitter vertical */
.${css.ClassV} {
    border: none;
}
/* Vertical Splitter separator */
.${css.ClassVsep} {
    width: 100%;
    cursor: ns-resize;
    background-color: white;
}
`  