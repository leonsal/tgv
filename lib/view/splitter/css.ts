import * as base from "../cssbase/css.js";

export const ClassH = base.Class + "_hsplit";
export const ClassHsep = ClassH + "_sep";

export const ClassV = base.Class + "_vsplit";
export const ClassVsep = ClassV + "_sep";
