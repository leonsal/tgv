import * as css from "./css.js";
import * as view from "../view/index.js";

export type Direction = "h" | "v";
export const Horizontal = "h";
export const Vertical = "v";
export type Options = {
    sepSize: number;    // Separator size in pixels
}
const defaultSepSize = 2;

export class Splitter extends view.View {

    protected _dir: Direction;
    protected _v0: view.View;
    protected _v1: view.View;
    protected _sep: view.View;
    protected _sepSize: number;
    protected _lastX: number;
    protected _lastY: number;
    protected _pos:   number;

    constructor(dir: Direction, v0: view.View, v1: view.View, options?: Options) {

        super("div");
        this._dir = dir;
        this._v0 = v0;
        this._v1 = v1; 
        if (options && options.sepSize != undefined) {
            this._sepSize = options.sepSize;
        } else {
            this._sepSize = defaultSepSize;
        }
        this._lastX = 0;
        this._lastY = 0;
        this._pos = 0.5
        this._sep = new Separator(dir, this._sepSize);

        if (dir == Horizontal) {
            this._el.className = css.ClassH;
            this._v0.el.style.cssFloat = "left";
            this._sep.el.style.cssFloat = "left";
            this._v1.el.style.cssFloat = "left";
        } else {
            this._el.className = css.ClassV;
        }

        this.appendChild(this._v0);
        this.appendChild(this._sep);
        this.appendChild(this._v1);

        this._setSizes(this._pos, this._sepSize);

        this._onMouseDown = this._onMouseDown.bind(this);
        this._onMouseMove = this._onMouseMove.bind(this);
        this._onMouseUp = this._onMouseUp.bind(this);
        this._sep.listen(view.MouseDown, this._onMouseDown);
    }

    setSplit(pos: number) {

        this._pos = pos < 0 ? 0 : pos > 1 ? 1 : pos;
        this._setSizes(this._pos, this._sepSize);
    }

    protected _onMouseDown(ev: Event) {

        // Save current mouse position and adds event listeners
        const mev = <MouseEvent>ev;
        this._lastX = mev.clientX;
        this._lastY = mev.clientY; 
        window.addEventListener(view.MouseMove, this._onMouseMove);
        window.addEventListener(view.MouseUp, this._onMouseUp);

        // Sets the global cursor the sames as the separator
        const style = getComputedStyle(this._sep.el);
        const cursor = style.getPropertyValue("cursor");
        this._setGlobalCursor(cursor);

        ev.preventDefault();
    }

    protected _onMouseMove(ev: Event) {

        const mev = <MouseEvent>ev;
        if (this._dir == Horizontal) {
            const delta = mev.clientX - this._lastX;
            let newPos = this._pos + (delta / this.el.clientWidth);
            this._setPos(newPos);
            this._lastX = mev.clientX;
        } else {
            const delta = mev.clientY - this._lastY;
            let newPos = this._pos + (delta / this.el.clientHeight);
            this._setPos(newPos);
            this._lastY = mev.clientY;
        }
        ev.preventDefault();
    }

    protected _onMouseUp(ev: Event) {

        window.removeEventListener(view.MouseMove, this._onMouseMove);
        window.removeEventListener(view.MouseUp, this._onMouseUp);
        this._setGlobalCursor("default");
    }

    protected _setPos(pos: number) {

        if (this._dir == Horizontal) {
            const posMin = (this._sepSize/2) / this.el.clientWidth; 
            const posMax = (this.el.clientWidth - this._sepSize/2) / this.el.clientWidth;
            if (pos < posMin) {
                pos = 0;
                this._pos = posMin;
            } else if (pos > posMax) {
                pos = 1;
                this._pos = posMax;
            } else {
                this._pos = pos;
            }
            this._setSizes(pos, this._sepSize);
        } else {
            const posMin = (this._sepSize/2) / this.el.clientHeight; 
            const posMax = (this.el.clientHeight - this._sepSize/2) / this.el.clientHeight;
            if (pos < posMin) {
                pos = 0;
                this._pos = posMin;
            } else if (pos > posMax) {
                pos = 1;
                this._pos = posMax;
            } else {
                this._pos = pos;
            }
            this._setSizes(pos, this._sepSize);
        }
    }

    protected _setSizes(pos: number, hwidth: number) {

        let v0Size: string;
        let v1Size: string;
        if (pos <= 0) {
            v0Size = "0%";
            v1Size = `calc(100% - ${hwidth}px)`
        } else if (pos >= 1) {
            v0Size = `calc(100% - ${hwidth}px)`;
            v1Size = "0%";
        } else {
            const posc = 100 * pos;
            const half = hwidth/2;
            v0Size = `calc(${posc}% - ${half}px)`;
            v1Size = `calc(${100-posc}% - ${half}px)`;
        }

        if (this._dir == Horizontal) {
            this._v0.el.style.width = v0Size;
            this._v1.el.style.width = v1Size;
        } else {
            this._v0.el.style.height = v0Size;
            this._v1.el.style.height = v1Size;
        }
    }

    protected _setGlobalCursor(cursor: string) {

        const html = document.getElementsByTagName("html")[0];
        html.style.cursor = cursor;
    }
}

class Separator extends view.View {

    constructor(dir: Direction, size: number) {

        super("div");
        if (dir == Horizontal) {
            this._el.className = css.ClassH + "_sep";
            this.el.style.width = `${size}px`;
        } else {
            this._el.className = css.ClassV + "_sep";
            this.el.style.height = `${size}px`;
        }
    }
}