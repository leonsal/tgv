- menu
    - menu bar opens submenus only after first click
    - selection of menu bar items on mouse hover ?
    - timeout before opening submenu of submenu.
    - keyboad navigation. 
- window
    - increase resize trigger on the se corner.
    - resizable constraints.
    - layout of content area

- standard dialogs
- button tooltip
- splitter
    - New version with invisible separator
    - separator is positioned absolutely
- list
    - list item
- dropdown
    - dropdown item
- image
- tabbar
    - tabpage
- form
- calendar
    - date input

- table
    - header
    - row 

- Sincronizacao Javascript / CSS
    - Cada componente e constituido pelos arquivos:
        - comp.ts           // implementacao
        - comp_css.ts       // define nomes das classes e variaveis css
        - comp_css_light.ts // define template do css para o tema: light
        - comp_css_dark.ts  // define template do css para o tema: dark
    - Ferramenta genthemes le o diretorio de componentes e gera os
      arquivos .css para os temas disponiveis.
    - Os arquivo common_css.ts and first_css_<theme>.ts e adicionado no inicio
    - Multiplas :root sections sao unidas ?
    :root {
    --var1: varvalue
    }
    :root {
    --var2: varvalue
    }
- mudar widget -> view



// BUILDER

interface BuildView {
    type:   string
}

const view = Builder({
    view:       "flex",
    direction:  "row",
    alignItems: "center",
    style: {
        margin: "10px",
    }
    items: [
        {
            view:   "button",
            name:   "btnDel",
            text:   tr("ok"),
            icon:   "check",
            onclick:    function() {},
        },
        { view: "gap", size: "10px" },
    ]
});

btnDel = view.find("flex/btnDel");

