import * as fs from "fs";
import * as path from "path";

// Type for the object with information needed for the template rendering
type Info =  {
    outDir:  string,
    themes:  {[theme:string]: {modPath: string, mod:any}[]}
};

let verbose = false;

function main() {

    // Parse command line parameters
    const args =  process.argv.splice(2);
    const inpdirs: string[] = [];
    let outCss = ".";
    let i = 0;
    while (i < args.length) {
        if (args[i] == "-v") {
            verbose = true;
            i++;
            continue;
        }
        if (args[i] == "-out" && i < args.length-1) {
            i++;
            outCss = args[i];
            i++;
            continue;
        }
        if (args[i].startsWith("-")) {
            usage();
            return;
        }
        inpdirs.push(args[i]);
        i++
    }
    if (inpdirs.length < 1) {
        usage();
        return;
    }

    // Convert input directories to absolute paths
    for (let i = 0; i < inpdirs.length; i++) {
        inpdirs[i] = path.resolve(inpdirs[i]);
    }

    // Get option for the directory of the generated css files
    outCss = path.resolve(outCss)

    // Checks input directories
    for (let dir of inpdirs) {
        let stat: fs.Stats;
        try {
            stat = fs.statSync(dir);
        } catch {
            console.log(`Input directory: ${dir} not found`);
            return;
        }
        if (!stat.isDirectory) {
            console.log(`Input directory: ${dir} is not a directory`);
            return;
        }
    }

    // Process input directories building info object
    const info: Info = { 
        outDir:     outCss,
        themes:     {},
    };
    for (const dir of inpdirs) {
        processDir(info, dir);
    }
    //console.log(info);

    loadModules(info, (msg:string) => {
       if (msg) {
           console.log(msg);
            return;
       }
       gencss(info);
    })
}

// Shows program usage
function usage() {

    const text = `
 Generates css theme files
 >gencss <options> input_dir <input_dir>  
 options:
 -out:    css output directory (default .)
 -v:      verbose output
`
    console.log(text);
}

// Process input directory calling processView()
// for each of its subdirectories.
function processDir(info: Info, dir: string) {

    if (verbose) {
        console.log("processing input dir:", dir);
    }
    const viewdirs = fs.readdirSync(dir);
    for (const vdir of viewdirs) {
        const vdirpath = path.join(dir, vdir);
        const stat = fs.statSync(vdirpath);
        if (!stat.isDirectory()) {
            continue;
        }
        processView(info, vdirpath);
    }
}

// Process a view subdirectory which could have the files
// css.ts - definition of css class name variables
// <theme>.css - none or any themes css files.
function processView(info: Info, vdir: string) {

    if (verbose) {
        console.log("processing view:", vdir)
    }

    // Read all files from the view directory and
    // checks if view contains the css_<theme>.js files.
    const files = fs.readdirSync(vdir);
    const vdirParsed = path.parse(vdir);

    for (const fname of files) {
        if (!fname.startsWith("css_") || !fname.endsWith(".js")) {
            continue;
        }
        // Get the css theme name from the file
        const fnameParsed = path.parse(fname);
        const parts = fnameParsed.name.split("_");
        const theme = parts[1];
        if (info.themes[theme] == undefined) {
            info.themes[theme] = [];
        }
        const modInfo = {modPath: path.join(vdir, fname), mod: null}
        if (vdirParsed.name == "cssbase") {
            info.themes[theme].unshift(modInfo);
        } else {
            info.themes[theme].push(modInfo);
        }
    }
}

// Load all themes modules asynchronously
function loadModules(info: Info, cb: (err:string)=>void){

    const themes = Object.keys(info.themes);
    const load = (ti: number, mi: number) => {

        if (ti >= themes.length) {
            cb("");
            return;

        }
        let theme = themes[ti];
        let mods = info.themes[theme];
        if (mi >= mods.length) {
            mi = 0;
            ti++;
            if (ti >= themes.length) {
                cb("");
                return;
            }
            theme = themes[ti];
            mods = info.themes[theme];
        }

        const modInfo = mods[mi]
        import(modInfo.modPath).then((m:any) => {
            modInfo.mod = m;
            load(ti, ++mi);
        })
    }
    load(0, 0);
}

function gencss(info: Info) {

    for (const theme in info.themes) {
        // Get the text for each css module
        const text: string[] = [];
        for (const modInfo of info.themes[theme])  {
            text.push(modInfo.mod.text);
            text.push("\n\n");
        }

        // Writes the css file for the theme
        const progPath = path.join(info.outDir, `theme_${theme}.css`);
        const fdesc = fs.openSync(progPath, "w");
        fs.writeSync(fdesc, text.join(""));
        fs.closeSync(fdesc);
        console.log("Generated css file:", progPath);
    }
}

main();
