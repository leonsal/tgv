
export const StyleNormal        = "normal";
export const StyleItalic        = "italic";
export const StyleOblique       = "oblique";
export const WeightNormal       = "normal";
export const WeightBold         = "bold";
export const WeightBolder       = "bolder";
export const WeightLighter      = "lighter";
export const Weight100          = "100";
export const Weight200          = "200";
export const Weight300          = "300";
export const Weight400          = "400";
export const Weight500          = "500";
export const Weight600          = "600";
export const Weight700          = "700";
export const Weight800          = "800";
export const Weight900          = "900";
export const UnitPx             = "px";
export const UnitPt             = "pt";
export const FamilySerif        = "serif";
export const FamilySansSerif    = "sans-serif";
export const FamilyCursive      = "cursive";
export const FamilyFantasy      = "fantasy";
export const FamilyMonospace    = "monospace";

export function New(size: number, unit=UnitPx, family=FamilySerif) {

    return Object.seal(new Font(size, unit, family));
}

export function clone(font: { size: number; }) {

    const f = New(font.size);
    return Object.assign(f, font);
}

var ctx: CanvasRenderingContext2D|null = null;

export class Font {

    protected _style:     string;
    protected _weight:    string;
    protected _size:      number;
    protected _sizeUnit:  string;
    protected _family:    string;
    protected _str:       string;
    protected _height:    number;

    constructor(size: number, unit=UnitPx, family=FamilySerif) {

        this._style     = StyleNormal;      // normal, italic, oblique
        this._weight    = WeightNormal;     // normal, bold, bolder, lighter, 100,...,900
        this._size      = size;             // size in pixels or points
        this._sizeUnit  = unit;             // px or pt
        this._family    = family;           // serif, sans-serif, cursive, fantasy, monospace, others
        this._str       = "";               // cached formatted font string
        this._height    = 0;                // cached calculated height
    }

    style() {

        return this._style;
    }

    setStyle(style: string) {

        this._style = style;
        this._reset();
    }

    weight() {

        return this._weight;
    }

    setWeight(weight: string) {

        this._weight = weight;
        this._reset();
    }

    size() {

        return this._size;
    }

    setSize(size: number) {

        this._size = size;
        this._reset();
    }

    sizeUnit() {

        return this._sizeUnit;
    }

    setSizeUnit(unit: string) {

        this._sizeUnit = unit;
        this._reset();
    }

    family() {

        return this._family;
    }

    setFamily(family: string) {

        this._family = family;
        this._reset();
    }

    setFrom(other: object) {

        Object.assign(this, other);
    }

    clone() {
        
        const c = New(this._size);
        return Object.assign(c, this);
    }

    toString() { 

        if (this._str == "") {
            this._str = [
                this._style,
                this._weight,
                this._size.toString() + this._sizeUnit,
                this._family,
            ].join(" ");
        }
        return this._str;
    }

    // Returns the width of the specified text using this font
    width(text: string) {

        if (ctx === null) {
            let canvas: HTMLCanvasElement = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            if (ctx == null) {
                throw new Error("getContext('2d')")
            }
        }
        ctx.font = this.toString();
        const tm = ctx.measureText(text);
        return tm.width;
    }

    height() {

        if (this._height != 0) {
            return this._height;
        }

        const body = document.getElementsByTagName("body")[0];
        const div = document.createElement("div");
        div.innerHTML = "M";
        const style = div.style;
        style.position      = "absolute";
        style.top           = "-100px";
        style.left          = "-100px";
        style.fontFamily    = this._family;
        style.fontWeight    = this._weight;
        style.fontSize      = this._size.toString() + this._sizeUnit;

        body.appendChild(div);
        const result = div.offsetHeight;
        body.removeChild(div);

        this._height = result;
        return this._height;
    }

    _reset() {
        this._str = "";
        this._height = 0;
    }
}

