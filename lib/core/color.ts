

export function Name(name: string) :Color {

    const rgb = colorName2RGB.get(name);
    if (rgb == undefined) {
        throw new Error("Invalid color name");
    }
    return RGBA(rgb.r, rgb.g, rgb.b, 1.0);
}

export function RGBA(r=0, g=0, b=0, a=0): Color {

    return Object.seal(new Color(r,g,b,a));
}


export class Color {

    _r:   number;
    _g:   number;
    _b:   number;
    _a:   number;
    _str: string;

    constructor(r=0, g=0, b=0, a=0) {

        this._r = r;
        this._g = g;
        this._b = b;
        this._a = a;
        this._str = "";
    }

    // This property returns the Red component of this color
    get r() {
        return this._r;
    }

    set r(v) {
        this._r = v;
        this._str = "";
    }

    get g() {
        return this._r;
    }

    set g(v) {
        this._g = v;
        this._str = "";
    }

    get b() {
        return this._b;
    }

    set b(v) {
        this._b = v;
        this._str = "";
    }

    get a() {
        return this._a;
    }

    set a(v) {
        this._a = v;
        this._str = "";
    }

    clone() {

        const c = RGBA(0,0,0,0);
        return Object.assign(c, this);
    }

    toString() {
       
        if (this._str !== "") {
            return this._str;
        }
        const ri = Math.round(255 * this._r);
        const gi = Math.round(255 * this._g);
        const bi = Math.round(255 * this._b);
        this._str = "rgba(" +
             ri.toString() + "," +
             gi.toString() + "," +
             bi.toString() + "," +
             this._a.toFixed(3) + 
        ")";
        return this._str;
    }

    setFrom(src: { _r: number; _g: number; _b: number; _a: number; _str: string; }) {

        this._r = src._r;
        this._g = src._g;
        this._b = src._b;
        this._a = src._a;
        this._str = src._str;
    }

    mult(v: number) {

        this._r *= v;
        this._g *= v;
        this._b *= v;
        this._str = "";
    }
}

// Maps standard web color names to an object with its rgb components
const colorName2RGB = new Map([
    ["aliceblue",           {r:0.941, g:0.973, b:1.000}],
    ["antiquewhite",        {r:0.980, g:0.922, b:0.843}],
    ["aqua",                {r:0.000, g:1.000, b:1.000}],
    ["aquamarine",          {r:0.498, g:1.000, b:0.831}],
    ["azure",               {r:0.941, g:1.000, b:1.000}],
    ["beige",               {r:0.961, g:0.961, b:0.863}],
    ["bisque",              {r:1.000, g:0.894, b:0.769}],
    ["black",               {r:0.000, g:0.000, b:0.000}],
    ["blanchedalmond",      {r:1.000, g:0.922, b:0.804}],
    ["blue",                {r:0.000, g:0.000, b:1.000}],
    ["blueviolet",          {r:0.541, g:0.169, b:0.886}],
    ["brown",               {r:0.647, g:0.165, b:0.165}],
    ["burlywood",           {r:0.871, g:0.722, b:0.529}],
    ["cadetblue",           {r:0.373, g:0.620, b:0.627}],
    ["chartreuse",          {r:0.498, g:1.000, b:0.000}],
    ["chocolate",           {r:0.824, g:0.412, b:0.118}],
    ["coral",               {r:1.000, g:0.498, b:0.314}],
    ["cornflowerblue",      {r:0.392, g:0.584, b:0.929}],
    ["cornsilk",            {r:1.000, g:0.973, b:0.863}],
    ["crimson",             {r:0.863, g:0.078, b:0.235}],
    ["cyan",                {r:0.000, g:1.000, b:1.000}],
    ["darkblue",            {r:0.000, g:0.000, b:0.545}],
    ["darkcyan",            {r:0.000, g:0.545, b:0.545}],
    ["darkgoldenrod",       {r:0.722, g:0.525, b:0.043}],
    ["darkgray",            {r:0.663, g:0.663, b:0.663}],
    ["darkgreen",           {r:0.000, g:0.392, b:0.000}],
    ["darkgrey",            {r:0.663, g:0.663, b:0.663}],
    ["darkkhaki",           {r:0.741, g:0.718, b:0.420}],
    ["darkmagenta",         {r:0.545, g:0.000, b:0.545}],
    ["darkolivegreen",      {r:0.333, g:0.420, b:0.184}],
    ["darkorange",          {r:1.000, g:0.549, b:0.000}],
    ["darkorchid",          {r:0.600, g:0.196, b:0.800}],
    ["darkred",             {r:0.545, g:0.000, b:0.000}],
    ["darksalmon",          {r:0.914, g:0.588, b:0.478}],
    ["darkseagreen",        {r:0.561, g:0.737, b:0.561}],
    ["darkslateblue",       {r:0.282, g:0.239, b:0.545}],
    ["darkslategray",       {r:0.184, g:0.310, b:0.310}],
    ["darkslategrey",       {r:0.184, g:0.310, b:0.310}],
    ["darkturquoise",       {r:0.000, g:0.808, b:0.820}],
    ["darkviolet",          {r:0.580, g:0.000, b:0.827}],
    ["deeppink",            {r:1.000, g:0.078, b:0.576}],
    ["deepskyblue",         {r:0.000, g:0.749, b:1.000}],
    ["dimgray",             {r:0.412, g:0.412, b:0.412}],
    ["dimgrey",             {r:0.412, g:0.412, b:0.412}],
    ["dodgerblue",          {r:0.118, g:0.565, b:1.000}],
    ["firebrick",           {r:0.698, g:0.133, b:0.133}],
    ["floralwhite",         {r:1.000, g:0.980, b:0.941}],
    ["forestgreen",         {r:0.133, g:0.545, b:0.133}],
    ["fuchsia",             {r:1.000, g:0.000, b:1.000}],
    ["gainsboro",           {r:0.863, g:0.863, b:0.863}],
    ["ghostwhite",          {r:0.973, g:0.973, b:1.000}],
    ["gold",                {r:1.000, g:0.843, b:0.000}],
    ["goldenrod",           {r:0.855, g:0.647, b:0.125}],
    ["gray",                {r:0.502, g:0.502, b:0.502}],
    ["green",               {r:0.000, g:0.502, b:0.000}],
    ["greenyellow",         {r:0.678, g:1.000, b:0.184}],
    ["grey",                {r:0.502, g:0.502, b:0.502}],
    ["honeydew",            {r:0.941, g:1.000, b:0.941}],
    ["hotpink",             {r:1.000, g:0.412, b:0.706}],
    ["indianred",           {r:0.804, g:0.361, b:0.361}],
    ["indigo",              {r:0.294, g:0.000, b:0.510}],
    ["ivory",               {r:1.000, g:1.000, b:0.941}],
    ["khaki",               {r:0.941, g:0.902, b:0.549}],
    ["lavender",            {r:0.902, g:0.902, b:0.980}],
    ["lavenderblush",       {r:1.000, g:0.941, b:0.961}],
    ["lawngreen",           {r:0.486, g:0.988, b:0.000}],
    ["lemonchiffon",        {r:1.000, g:0.980, b:0.804}],
    ["lightblue",           {r:0.678, g:0.847, b:0.902}],
    ["lightcoral",          {r:0.941, g:0.502, b:0.502}],
    ["lightcyan",           {r:0.878, g:1.000, b:1.000}],
    ["lightgoldenrodyellow",{r:0.980, g:0.980, b:0.824}],
    ["lightgray",           {r:0.827, g:0.827, b:0.827}],
    ["lightgreen",          {r:0.565, g:0.933, b:0.565}],
    ["lightgrey",           {r:0.827, g:0.827, b:0.827}],
    ["lightpink",           {r:1.000, g:0.714, b:0.757}],
    ["lightsalmon",         {r:1.000, g:0.627, b:0.478}],
    ["lightseagreen",       {r:0.125, g:0.698, b:0.667}],
    ["lightskyblue",        {r:0.529, g:0.808, b:0.980}],
    ["lightslategray",      {r:0.467, g:0.533, b:0.600}],
    ["lightslategrey",      {r:0.467, g:0.533, b:0.600}],
    ["lightsteelblue",      {r:0.690, g:0.769, b:0.871}],
    ["lightyellow",         {r:1.000, g:1.000, b:0.878}],
    ["lime",                {r:0.000, g:1.000, b:0.000}],
    ["limegreen",           {r:0.196, g:0.804, b:0.196}],
    ["linen",               {r:0.980, g:0.941, b:0.902}],
    ["magenta",             {r:1.000, g:0.000, b:1.000}],
    ["maroon",              {r:0.502, g:0.000, b:0.000}],
    ["mediumaquamarine",    {r:0.400, g:0.804, b:0.667}],
    ["mediumblue",          {r:0.000, g:0.000, b:0.804}],
    ["mediumorchid",        {r:0.729, g:0.333, b:0.827}],
    ["mediumpurple",        {r:0.576, g:0.439, b:0.859}],
    ["mediumseagreen",      {r:0.235, g:0.702, b:0.443}],
    ["mediumslateblue",     {r:0.482, g:0.408, b:0.933}],
    ["mediumspringgreen",   {r:0.000, g:0.980, b:0.604}],
    ["mediumturquoise",     {r:0.282, g:0.820, b:0.800}],
    ["mediumvioletred",     {r:0.780, g:0.082, b:0.522}],
    ["midnightblue",        {r:0.098, g:0.098, b:0.439}],
    ["mintcream",           {r:0.961, g:1.000, b:0.980}],
    ["mistyrose",           {r:1.000, g:0.894, b:0.882}],
    ["moccasin",            {r:1.000, g:0.894, b:0.710}],
    ["navajowhite",         {r:1.000, g:0.871, b:0.678}],
    ["navy",                {r:0.000, g:0.000, b:0.502}],
    ["oldlace",             {r:0.992, g:0.961, b:0.902}],
    ["olive",               {r:0.502, g:0.502, b:0.000}],
    ["olivedrab",           {r:0.420, g:0.557, b:0.137}],
    ["orange",              {r:1.000, g:0.647, b:0.000}],
    ["orangered",           {r:1.000, g:0.271, b:0.000}],
    ["orchid",              {r:0.855, g:0.439, b:0.839}],
    ["palegoldenrod",       {r:0.933, g:0.910, b:0.667}],
    ["palegreen",           {r:0.596, g:0.984, b:0.596}],
    ["paleturquoise",       {r:0.686, g:0.933, b:0.933}],
    ["palevioletred",       {r:0.859, g:0.439, b:0.576}],
    ["papayawhip",          {r:1.000, g:0.937, b:0.835}],
    ["peachpuff",           {r:1.000, g:0.855, b:0.725}],
    ["peru",                {r:0.804, g:0.522, b:0.247}],
    ["pink",                {r:1.000, g:0.753, b:0.796}],
    ["plum",                {r:0.867, g:0.627, b:0.867}],
    ["powderblue",          {r:0.690, g:0.878, b:0.902}],
    ["purple",              {r:0.502, g:0.000, b:0.502}],
    ["red",                 {r:1.000, g:0.000, b:0.000}],
    ["rosybrown",           {r:0.737, g:0.561, b:0.561}],
    ["royalblue",           {r:0.255, g:0.412, b:0.882}],
    ["saddlebrown",         {r:0.545, g:0.271, b:0.075}],
    ["salmon",              {r:0.980, g:0.502, b:0.447}],
    ["sandybrown",          {r:0.957, g:0.643, b:0.376}],
    ["seagreen",            {r:0.180, g:0.545, b:0.341}],
    ["seashell",            {r:1.000, g:0.961, b:0.933}],
    ["sienna",              {r:0.627, g:0.322, b:0.176}],
    ["silver",              {r:0.753, g:0.753, b:0.753}],
    ["skyblue",             {r:0.529, g:0.808, b:0.922}],
    ["slateblue",           {r:0.416, g:0.353, b:0.804}],
    ["slategray",           {r:0.439, g:0.502, b:0.565}],
    ["slategrey",           {r:0.439, g:0.502, b:0.565}],
    ["snow",                {r:1.000, g:0.980, b:0.980}],
    ["springgreen",         {r:0.000, g:1.000, b:0.498}],
    ["steelblue",           {r:0.275, g:0.510, b:0.706}],
    ["tan",                 {r:0.824, g:0.706, b:0.549}],
    ["teal",                {r:0.000, g:0.502, b:0.502}],
    ["thistle",             {r:0.847, g:0.749, b:0.847}],
    ["tomato",              {r:1.000, g:0.388, b:0.278}],
    ["turquoise",           {r:0.251, g:0.878, b:0.816}],
    ["violet",              {r:0.933, g:0.510, b:0.933}],
    ["wheat",               {r:0.961, g:0.871, b:0.702}],
    ["white",               {r:1.000, g:1.000, b:1.000}],
    ["whitesmoke",          {r:0.961, g:0.961, b:0.961}],
    ["yellow",              {r:1.000, g:1.000, b:0.000}],
    ["yellowgreen",         {r:0.604, g:0.804, b:0.196}],
]);

