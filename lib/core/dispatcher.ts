//import * as event from "./event";
//
//// Type for dispatcher callbacks
//export type Callback = (ev: event.Event, ...args:any)=>void;
//
//
//export class Dispatcher {
//
//    private _events: Map<string, {cb:Callback, self?:any}[]>
//
//    constructor() {
//        this._events = new Map();
//    }
//
//    // Subscribe for the specified event calling the specified callback 
//    subscribe(evname: string, cb: Callback, ...args:any) {
//
//        this.subscribeThis(evname, null, cb, args);
//    }
//
//    // Unsubscribe for the specified event and callback
//    unsubscribe(evname: string, cb: Callback): number {
//
//        return this.unsubscribeThis(evname, null, cb)
//    }
//
//    // Subscribe for the specified event
//    subscribeThis(evname: string, self:any, cb: Callback, ...args:any) {
//
//        let subs = this._events.get(evname);
//        if (subs === undefined) {
//            subs = [];
//            this._events.set(evname, subs);
//        }
//        subs.push({cb, self});
//    }
//
//    // Unsubscribe for the specified event and callback.
//    // Returns the number of subscriptions found.
//    unsubscribeThis(evname: string, self:any, cb: Callback): number {
//
//        let found = 0;
//        const subs = this._events.get(evname);
//        if (subs === undefined) {
//            return found;
//        }
//        let i = 0;
//        while (i < subs.length) {
//            if (subs[i].cb !== cb || subs[i].self !== self) {
//                i++;
//                continue;
//            }
//            subs.splice(i, 1);
//            found++;
//        }
//        return found;
//    }
//
//    // Dispatch event calling all subscribed listeners and passing the
//    // supplied arguments
//    dispatch(ev: event.Event, ...args: any) {
//
//        const subs = this._events.get(ev.type);
//        if (subs === undefined) {
//            return;
//        }
//        for (let i = 0; i < subs.length; i++) {
//            const sub = subs[i];
//            if (sub.self) {
//                sub.cb.call(sub.self, ev, ...args)
//            } else {
//                sub.cb(ev, ...args);
//            }
//        }
//    }
//
//    clear() {
//
//    }
//}
//
//
//