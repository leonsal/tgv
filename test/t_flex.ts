import * as tests from "./tests.js";
import * as tgv from "../lib/index.js";

function control(root: HTMLElement, lc: tgv.flex.Flex) {

    const l1 = new tgv.flex.Flex("row");
    l1.setAlignItems("center");
    const gap: tgv.flex.Gap = {size: "10px"};
    l1.el.style.padding = "4px";
    const selMargins = "0px 2px 0px 10px";
    root.appendChild(l1.el);

    const selWrap = <tgv.select.Select>tgv.builder.build({
        view: "select",
        label: "FlewWrap: ",
        items: [{text: "nowrap"}, {text:"wrap"}, {text:"wrap-reverse"}],
        onChange: () => {
            const op = selWrap.selected();
            if (op) {
                console.log("FlexWrap:", op.value());
                lc.setWrap(<tgv.flex.Wrap>op.value());
            }
        }
    })
    //// Selects FlexWrap
    //const selWrap = new tgv.select.Select();
    //selWrap.setLabel("FlewWrap: ");
    //["nowrap", "wrap", "wrap-reverse"].forEach(
    //    (v) => selWrap.add(v,v));
    //selWrap.listen(tgv.view.Change, ()=> {
    //    const op = selWrap.selected();
    //    if (op) {
    //        console.log("FlexWrap:", op.value());
    //        lc.setWrap(<tgv.flex.Wrap>op.value());
    //    }
    //});
    l1.appendChild(selWrap);
    l1.appendGap(gap);

    // Selects AlignItems
    const selAlignItems = new tgv.select.Select();
    selAlignItems.setLabel("AlignItems:" );
    ["stretch", "flex-start", "flex-end", "center", "baseline"].forEach(
        (v) => selAlignItems.add(v,v));
    selAlignItems.listen(tgv.view.Change, () => {
        const op = selAlignItems.selected();
        if (op) {
            console.log("AlignItems:", op.value());
            lc.setAlignItems(<tgv.flex.AlignItems>op.value());
        }
    });
    l1.appendChild(selAlignItems);
    l1.appendGap(gap);

    // Selects AlignContent
    const selAlignContent = new tgv.select.Select();
    selAlignContent.setLabel("AlignContent: ");
    ["flex-start", "flex-end", "center", "space-between", "space-around", "space-evenly"].forEach(
        (v) => selAlignContent.add(v, v));
    selAlignContent.listen(tgv.view.Change, () => {
        const op = selAlignContent.selected();
        if (op) {
            console.log("AlignContent:", op.value());
            lc.setAlignContent(<tgv.flex.AlignContent>op.value());
        }
    });
    l1.appendChild(selAlignContent);
    l1.appendGap(gap);

    // Selects JustifyContent
    const selJustifyContent = new tgv.select.Select();
    selJustifyContent.setLabel("JustifyContent: ");
    ["flex-start", "flex-end", "center", "space-between", "space-around", "space-evenly"].forEach(
        (v) => selJustifyContent.add(v, v));
    selJustifyContent.listen(tgv.view.Change, () => {
        const op = selJustifyContent.selected();
        if (op) {
            console.log("JustifyContent:", op.value());
            lc.setJustifyContent(<tgv.flex.JustifyContent>op.value());
        }
    });
    l1.appendChild(selJustifyContent);
    l1.appendGap(gap);

    // Grid overflow
    const selOverflow = new tgv.select.Select();
    selOverflow.setLabel("Overflow: ");
    ["auto", "scroll", "hidden", "visible"].forEach(
        (v) => selOverflow.add(v,v));
    selOverflow.listen(tgv.view.Change, () => {
        const op = selOverflow.selected();
        if (op) {
            console.log("Overflow:", op.value());
            lc.el.style.overflow = op.value();
        }
    });
    l1.appendChild(selOverflow);

    // Child alignment
    const l2 = new tgv.flex.Flex("row");
    l2.setAlignItems("center");
    l2.el.style.padding = "4px";
    root.appendChild(l2.el);

    // Selects AlignSelf
    const selAlignSelf = new tgv.select.Select();
    selAlignSelf.setLabel("AlignSelf: ");
    ["auto", "flex-start", "flex-end", "center", "baseline", "stretch"].forEach(
        (v) => selAlignSelf.add(v,v));
    selAlignSelf.listen(tgv.view.Change, () => {
        const op = selAlignSelf.selected();
        if (op) {
            console.log("AlignSelf:", op.value());
            setSelectedChild();
        }
    });
    l2.appendChild(selAlignSelf);
    l2.appendGap(gap);

    // Selects FlexGrow
    const selFlexGrow = new tgv.select.Select();
    selFlexGrow.setLabel("FlexGrow: ");
    for (let v = 0; v < 10; v++) {
        const s = v.toString();
        selFlexGrow.add(s, s);
    }
    selFlexGrow.listen(tgv.view.Change, () => {
        setSelectedChild();
    });
    l2.appendChild(selFlexGrow);
    l2.appendGap(gap);

    // Selects FlexShrink
    const selFlexShrink = new tgv.select.Select();
    selFlexShrink.setLabel("FlexShrink: ");
    for (let v = 0; v < 10; v++) {
        const s = v.toString();
        selFlexShrink.add(s, s);
    }
    selFlexShrink.listen(tgv.view.Change, () => {
        setSelectedChild();
    });
    l2.appendChild(selFlexShrink);
    l2.appendGap(gap);

    // Selects Order
    const selOrder = new tgv.select.Select();
    selOrder.setLabel("Order: ");
    for (let v = 0; v < 10; v++) {
        const s = v.toString();
        selOrder.add(s, s);
    }
    selOrder.listen(tgv.view.Change, () => {
        setSelectedChild();
    });
    l2.appendChild(selOrder);

    // Set child flex properties
    const setChild = function(child: tgv.checkbox.Checkbox) {
        const opAlignSelf = selAlignSelf.selected();
        if (opAlignSelf) {
            child.el.style.alignSelf = opAlignSelf.value();
        }
        const opFlexGrow = selFlexGrow.selected();
        if (opFlexGrow) {
            child.el.style.flexGrow = opFlexGrow.value()
        }
        const opFlexShrink = selFlexShrink.selected();
        if (opFlexShrink) {
            child.el.style.flexShrink = opFlexShrink.value()
        }
        const opOrder = selOrder.selected();
        if (opOrder) {
            child.el.style.order = opOrder.value()
        }
    }

    // Set selected child properties
    const setSelectedChild = function() {

        for (const c of lc.children()) {
            const cb = <tgv.checkbox.Checkbox>c;
            if (cb.value()) {
                setChild(cb);
            }
        }
    }

    // Adds child button
    let count = 1;
    const bAddChild = new tgv.button.Button("Add child");
    bAddChild.el.style.margin = selMargins;
    bAddChild.el.style.color = "blue";
    bAddChild.listen(tgv.view.Click, () => {
        const child = new tgv.checkbox.Checkbox("Child" + count.toString());
        child.el.style.border = "1px solid gray";
        child.el.style.backgroundColor = "lightgray";
        setChild(child);
        count++;
        lc.appendChild(child);
    });
    l2.appendChild(bAddChild);

    // Remove selected children button
    const bRemChild = new tgv.button.Button("Remove Selected");
    bRemChild.el.style.margin = selMargins;
    bRemChild.listen(tgv.view.Click, () => {
        let pos = 0;
        while (pos < lc.children().length) {
            const cb = <tgv.checkbox.Checkbox>lc.children()[pos];
            if (cb.value()) {
                lc.removeChild(cb);
                continue;
            }
            pos++;
        }
    });
    l2.appendChild(bRemChild);


}

function run() {

    // Root container
    const root = <HTMLElement>document.getElementById("root");
    root.style.backgroundColor = "whitesmoke";

    // Horizontal flex layout container
    const lh = new tgv.flex.Flex("row");
    lh.el.style.marginTop = "10px";
    lh.el.style.marginBottom = "10px";
    lh.el.style.height = "200px";
    lh.el.style.border = "1px solid black";
    lh.el.style.backgroundColor = "white";
    lh.el.style.overflow = "auto";
    control(root, lh)
    root.appendChild(lh.el);

    // Vertical flex layout container
    const lv = new tgv.flex.Flex("column");
    lv.el.style.marginTop = "10px";
    lv.el.style.height = "560px";
    lv.el.style.width = "300px";
    lv.el.style.border = "1px solid black";
    lv.el.style.backgroundColor = "white";
    lv.el.style.overflow = "auto";
    control(root, lv)
    root.appendChild(lv.el);
}

tests.All.set("flex", run);
