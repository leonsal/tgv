import * as tests from "./tests.js";
import * as tgv from "../lib/index.js";

function run() {

    const root = <HTMLElement>document.getElementById("root");

    const t1 = new tgv.tuner.Tuner(8, "Hz", 12_345_678);
    t1.listen(tgv.view.Change, (v:any) => console.log("t1 changed:", v.detail));
    root.appendChild(t1.el);

    const t2 = new tgv.tuner.Tuner(4, " Hz", 123);
    t2.listen(tgv.view.Change, (v:any) => console.log("t22 changed:", v.detail));
    t2.el.style.fontSize = "12px";
    root.appendChild(t2.el);
}

tests.All.set("tuner", run);