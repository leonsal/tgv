import * as tests from "./tests.js";
import * as tgv from "../lib/index.js";


function run()  {

    // Root container
    const root = <HTMLElement>document.getElementById("root");
    root.style.backgroundColor = "whitesmoke";
    const gap: tgv.flex.Gap = {size: "10px"};

    // First row of controls
    const r1 = new tgv.flex.Flex("row");
    r1.setAlignItems("center");
    r1.el.style.padding = "8px";
    root.appendChild(r1.el);

    // Grid template text input
    const t1 = new tgv.text.Text("none / repeat(6, auto)");
    t1.setPlaceholder("Grid template");
    t1.setLabel("Template: ");
    t1.setWidth("260px");
    t1.listen(tgv.text.OnEnter, () => {
        console.log("template:", t1.value());
        g.setTemplate(t1.value());
    });
    r1.appendChild(t1);
    r1.appendGap(gap);

    // Grid row gap
    const t2 = new tgv.text.Text("0px");
    t2.setPlaceholder("Grid row-gap");
    t2.setLabel("RowGap: ");
    t2.setWidth("60px");
    t2.listen(tgv.text.OnEnter, () => {
        console.log("row-gap:", t2.value());
        g.setRowGap(t2.value());
    });
    r1.appendChild(t2);
    r1.appendGap(gap);

    // Grid column gap
    const t3 = new tgv.text.Text("0px");
    t3.setPlaceholder("Grid column-gap");
    t3.setLabel("ColumnGap: ");
    t3.setWidth("60px");
    t3.listen(tgv.text.OnEnter, () => {
        console.log("column-gap:", t3.value());
        g.setColumnGap(t3.value());
    });
    r1.appendChild(t3);
    r1.appendGap(gap);

    // Grid overflow
    const selOverflow = new tgv.select.Select();
    selOverflow.setLabel("Overflow: ");
    const selOverflowOptions = ["auto", "scroll", "hidden", "visible"];
    selOverflowOptions.forEach((v) => selOverflow.add(v,v));
    selOverflow.listen(tgv.view.Change, () => {
        const op = selOverflow.selected();
        if (op) {
            console.log("Overflow:", op.value());
            g.el.style.overflow = op.value();
        }
    });
    r1.appendChild(selOverflow);

    // Second  controls row layout
    const r2 = new tgv.flex.Flex("row");
    r2.setAlignItems("center");
    r2.el.style.padding = "8px";
    root.appendChild(r2.el);

    // Selects AlignItems
    const selAlignItems = new tgv.select.Select();
    selAlignItems.setLabel("AlignItems: ");
    const selAlignItemsOptions = [
        "auto", "normal", "start", "end", "center", "stretch", "baseline", "first baseline", "last baseline"];
    selAlignItemsOptions.forEach((v) => selAlignItems.add(v,v));
    selAlignItems.setSelected("stretch");
    selAlignItems.listen(tgv.view.Change, () => {
        const op = selAlignItems.selected();
        if (op) {
            console.log("AlignItems:", op.value());
            g.setAlignItems(<tgv.grid.AlignItems>op.value());
        }
    });
    r2.appendChild(selAlignItems);
    r2.appendGap(gap);

    // Selects JustifyItems
    const selJustifyItems = new tgv.select.Select();
    selJustifyItems.setLabel("JustifyItems: ");
    selAlignItemsOptions.forEach((v) => selJustifyItems.add(v,v));
    selJustifyItems.setSelected("stretch");
    selJustifyItems.listen(tgv.view.Change, () => {
        const op = selJustifyItems.selected();
        if (op) {
            console.log("JustifyItems:", op.value());
            g.setJustifyItems(<tgv.grid.JustifyItems>op.value());
        }
    });
    r2.appendChild(selJustifyItems);
    r2.appendGap(gap);

    // Selects AlignContent
    const selAlignContent = new tgv.select.Select();
    selAlignContent.setLabel("AlignContent: ");
    const selAlignContentOptions = [
    "normal", "start", "end", "center", "stretch",
    "space-around", "space-between", "space-evenly",
    "baseline", "first baseline",  "last baseline"];
    selAlignContentOptions.forEach((v) => selAlignContent.add(v,v));
    selAlignContent.listen(tgv.view.Change, () => {
        const op = selAlignContent.selected();
        if (op) {
            console.log("AlignContent:", op.value());
            g.setAlignContent(<tgv.grid.AlignContent>op.value());
        }
    });
    r2.appendChild(selAlignContent);
    r2.appendGap(gap);

    // Selects JustifyContent
    const selJustifyContent = new tgv.select.Select();
    selJustifyContent.setLabel("JustifyContent: ");
    selAlignContentOptions.forEach((v) => selJustifyContent.add(v,v));
    selJustifyContent.listen(tgv.view.Change, () => {
        const op = selJustifyContent.selected();
        if (op) {
            console.log("JustifyContent:", op.value());
            g.setJustifyContent(<tgv.grid.JustifyContent>op.value());
        }
    });
    r2.appendChild(selJustifyContent);
    r2.appendGap(gap);

    // Selects AutoFlow
    const selAutoFlow = new tgv.select.Select();
    selAutoFlow.setLabel("AutoFlow: ");
    const selAutoFlowOptions = [
        "row", "column", "dense", "row dense", "column dense"];
    selAutoFlowOptions.forEach((v) => selAutoFlow.add(v,v));
    selAutoFlow.listen(tgv.view.Change, () => {
        const op = selAutoFlow.selected();
        if (op) {
            console.log("AutoFlow:", op.value());
            g.setAutoFlow(<tgv.grid.AutoFlow>op.value());
        }
    });
    r2.appendChild(selAutoFlow);
    r2.appendGap(gap);

    // Auto rows
    const r2t1 = new tgv.text.Text("auto");
    r2t1.setPlaceholder("grid-auto-rows");
    r2t1.setLabel("AutoRows: ");
    r2t1.setWidth("60px");
    r2t1.listen(tgv.text.OnEnter, () => {
        console.log("AutoRows:", r2t1.value());
        g.setAutoRows(r2t1.value());
    });
    r2.appendChild(r2t1);

    // Third controls row layout
    const r3 = new tgv.flex.Flex("row");
    r3.setAlignItems("center");
    r3.el.style.padding = "8px";
    root.appendChild(r3.el);

    // Selects AlignSelf
    const selAlignSelf = new tgv.select.Select();
    selAlignSelf.setLabel("AlignSelf: ");
    const selAlignSelfOptions = selAlignItemsOptions;
    selAlignSelfOptions.forEach((v) => selAlignSelf.add(v,v));
    selAlignSelf.listen(tgv.view.Change, () => {
        const op = selAlignSelf.selected();
        if (op) {
            console.log("AlignSelf:", op.value());
            setSelectedChild();
        }
    } )
    r3.appendChild(selAlignSelf);
    r3.appendGap(gap);

    // Selects AlignSelf
    const selJustifySelf = new tgv.select.Select();
    selJustifySelf.setLabel("JustifySelf: ");
    const selJustifySelfOptions = selAlignItemsOptions;
    selJustifySelfOptions.forEach((v) => selJustifySelf.add(v,v));
    selJustifySelf.listen(tgv.view.Change, () => {
        const op = selJustifySelf.selected();
        if (op) {
            setSelectedChild();
        }
    } )
    r3.appendChild(selJustifySelf);
    r3.appendGap(gap);

    // Grid area text input
    const r3t1 = new tgv.text.Text("auto/auto/auto/auto");
    r3t1.setPlaceholder("Child grid area");
    r3t1.setLabel("GridArea: ");
    r3t1.listen(tgv.text.OnEnter, () => {
        console.log("grid area:", r3t1.value());
        setSelectedChild();
    });
    r3.appendChild(r3t1);
    r3.appendGap(gap);

    // Set grid child properties
    const setChild = function(child: tgv.checkbox.Checkbox) {
        const opAlignSelf = selAlignSelf.selected();
        if (opAlignSelf) {
            console.log("AlignSelf:", opAlignSelf.value());
            child.el.style.alignSelf = opAlignSelf.value();
        }
        const opJustifySelf = selJustifySelf.selected();
        if (opJustifySelf) {
            console.log("JustifySelf:", opJustifySelf.value());
            child.el.style.justifySelf = opJustifySelf.value();
        }
        child.el.style.gridArea = r3t1.value();
    }

    // Set selected child properties
    const setSelectedChild = function() {

        for (const c of g.children()) {
            const cb = <tgv.checkbox.Checkbox>c;
            if (cb.value()) {
                setChild(cb);
            }
        }
    }

    // Adds child button
    let count = 1;
    const bAddChild = new tgv.button.Button("Add child");
    bAddChild.el.style.color = "blue";
    bAddChild.listen(tgv.view.Click, () => {
        const child = new tgv.checkbox.Checkbox("Child" + count.toString());
        child.el.style.border = "1px solid gray";
        child.el.style.backgroundColor = "lightgray";
        setChild(child);
        count++;
        g.appendChild(child);
    });
    r3.appendChild(bAddChild);
    r3.appendGap(gap);

    // Remove selected children button
    const bRemChild = new tgv.button.Button("Remove Selected");
    bRemChild.listen(tgv.view.Click, () => {
        let pos = 0;
        while (pos < g.children().length) {
            const cb = <tgv.checkbox.Checkbox>g.children()[pos];
            if (cb.value()) {
                g.removeChild(cb);
                continue;
            }
            pos++;
        }
    });
    r3.appendChild(bRemChild);

    // Grid layout container
    const g = new tgv.grid.Grid(t1.value());
    g.el.style.marginTop = "10px";
    g.el.style.marginBottom = "10px";
    g.el.style.height = "400px";
    g.el.style.border = "1px solid black";
    g.el.style.backgroundColor = "white";
    g.el.style.overflow = "auto";
    root.appendChild(g.el);
}

tests.All.set("grid", run);

