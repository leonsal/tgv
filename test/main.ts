import * as tests from "./tests.js";
import "./t_button.js";
import "./t_flex.js";
import "./t_grid.js";
import "./t_window.js";
import "./t_msgbox.js";
import "./t_splitter.js";
import "./t_menu.js";
import "./t_tuner.js";

function main() {

    console.log("starting");
    // Get optional test name from url
    let search = window.location.search;
    if (search.length === 0) {
        buildMenu();
        return;
    }

    // Get test module from test name
    let name = search.substring(3);
    let test = tests.All.get(name);
    if (!test) {
        console.log("Invalid test name:%s", name);
        return;
    }
    test();
}

function buildMenu() {

    let body = document.getElementsByTagName("body");
    for (const name of tests.All.keys()) {
        let link = document.createElement("a");
        link.href = "?t=" + name;
        link.style.display = "block";
        link.innerHTML = name;
        body[0].appendChild(link);
    }
}

window.onload = main;
