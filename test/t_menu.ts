import * as tests from "./tests.js";
import * as tgv from "../lib/index.js";

function run() {

    // Root container
    const root = new tgv.view.View("div");
    document.getElementsByTagName("body")[0].appendChild(root.el);

    // Menu
    const m0 = tgv.builder.build({
        view: "menu",
        items: [
            { label: "Item1 is a very long option name", icon: "zoom_in", id: "m01"},
            { label: "Item2", icon: "zoom_out", id: "m02", accel: "ctrl+1" },
            { label: "Item3", icon: "visibility", id: "m03", accel: "ctrl+shif+2" },
            { type: "separator" },
            { label: "Item4", icon: "zoom_in", id: "m04", accel: "alt+g" },
            { label: "sub menu1", icon: "visibility", menuSpec:{ items:[
                { label: "Sub item1", icon: "zoom_in", id: "m011"},
                { label: "Sub Item2", icon:"zoom_out", id: "m012"},
                { label: "Sub Item3", icon:"visibility", id: "m013", onClick:
                    (mi:tgv.menu.MenuItem)=>console.log("item click:", mi.id())
                },
                { type: "separator" },
                { label:"Sub Item4", icon:"zoom_in"},
            ]}},
            { label: "Item5", icon: "face", id: "m05", accel: "alt+k" },
        ],
        onItemClick: (ev:Event) => {
            const cev = <CustomEvent>ev;
            const mi = <tgv.menu.MenuItem>cev.detail;
            console.log("OnItemClick:", mi.id());
        },
    });
    root.appendChild(<tgv.view.View>m0);

    // Spacing
    root.appendChild(new tgv.view.View("br"));
    root.appendChild(new tgv.view.View("br"));

    const refs = {};
    // MenuBar
    const mh = tgv.builder.build({
        view: "menubar",
        items: [
            // Menu1
            {label: "Menu1", menuSpec:{ items: [
                {label: "Menu1 item1", icon: "zoom_in", id:"m11", visible: true, accel:"alt+a"},
                {label: "Menu1 item2", icon: "zoom_out", id:"m12"},
                {label: "Menu1 item3", icon: "visibility", enabled: false},
                {type: "separator"},
                {label: "Menu1 menu1", menuSpec: { items: [
                    {label: "Menu1.1 item1"},
                    {type: "separator"},
                    {type: "checkbox", label: "Menu1.1 item2"},
                    {type: "checkbox", label: "Menu1.1 item3", checked:true},
                    {type: "separator"},
                    {label: "Menu 1.1.1", icon: "zoom_in", menuSpec: { items: [
                        {type: "radio", label: "Menu1.1.1 item1"},
                        {type: "radio", label: "Menu1.1.1 item2"},
                        {type: "separator"},
                        {type: "radio", label: "Menu1.1.1 item3"},
                        {type: "radio", label: "Menu1.1.1 item4"},
                        {type: "radio", label: "Menu1.1.1 item5"},
                    ]}},
                ]}},
                {label: "Menu1 item4", visible: false},
                {label: "Menu1 menu2", menuSpec:{ items:[
                    {label: "Menu1.2 item1"},
                    {label: "Menu1.2 item2"},
                    {type: "separator"},
                    {label: "Menu1.2 item3"},
                    {label: "Menu1.2 item4"},
                ]}},
            ]}},
            // Menu2
            {label: "Menu2", menuSpec:{ items: [
                {label: "Menu2 item1", icon: "zoom_in"},
                {label: "Menu2 item2", icon: "zoom_out"},
                {type: "separator"},
                {label: "Menu1 item3", icon: "visibility"},
                {label: "Menu2 menu1", menuSpec:{ items:[
                    {label: "Menu2.1 item1"},
                    {type :"separator"},
                    {label: "Menu2.1 item2"},
                    {type: "separator"},
                    {label: "Menu2.1 item3"},
                    {label: "Menu2.1 item4"},
                ]}},
                {label: "Menu2 menu2", menuSpec: { items: [
                    {label: "Menu2.2 item1"},
                    {label: "Menu2.2 item2"},
                    {type: "separator"},
                    {label: "Menu2.2 item3"},
                ]}},
                {label: "Menu2 item4", icon: "visibility"},
            ]}},
            // Menu3
            {label: "Menu3", menuSpec:{ items: [
                {label: "Menu3 item1", icon: "zoom_in"},
                {type: "separator"},
                {label: "Menu3 item2", icon: "zoom_out"},
                {label :"Menu3 item3", icon: "visibility"},
            ]}},
            // Menu4
            {label: "Menu4", visible: false, id: "menu4", menuSpec:{ items: [
                {label: "Menu4 item1", icon: "zoom_in", enabled: false},
                {type: "separator"},
                {label: "Menu4 item2", icon: "zoom_in", enabled: true},
            ]}},
            // Menu5
            {label: "Menu5", menuSpec:{ items: [
                {label: "Menu5 item1", icon : "zoom_in"},
                {type: "separator"},
                {label: "Menu5 item2", icon : "zoom_out"},
                {label: "Menu5 item3", icon : "visibility"},
                {type: "separator"},
                {type: "checkbox", label: "Show Menu4", checked: false, onClick: (mi:tgv.menu.MenuItem)=>{
                    const menu4 = (<tgv.menu.Menu>mh).getItemById("menu4");
                    if (menu4 == null) {
                        return;
                    }
                    menu4.setVisible(mi.checked());
                }},
            ]}},
        ],
        onItemClick: (ev:Event) => {
            const mi = <tgv.menu.MenuItem>(<CustomEvent>ev).detail;
            console.log("MenuBar OnItemClick:", mi.id());
        },
    });
    root.appendChild(<tgv.view.View>mh);
}

tests.All.set("menu", run);