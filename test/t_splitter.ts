import * as tests from "./tests.js";
import * as tgv from "../lib/index.js";

function run() {

    // Root container
    const root = new tgv.view.View("div");
    document.getElementsByTagName("body")[0].appendChild(root.el);

    const h1 = new tgv.view.View("div");
    h1.el.style.backgroundColor = "blue";
    h1.el.style.height = "100%";
    h1.el.style.width = "100px";

    const h2 = new tgv.view.View("div");
    h2.el.style.backgroundColor = "green";
    h2.el.style.height = "100%";
    h2.el.style.width = "100px";

    const sh1 = new tgv.splitter.Splitter(tgv.splitter.Horizontal, h1, h2);
    sh1.setSplit(0.1);
    sh1.el.style.marginLeft = "50px";
    sh1.el.style.border = "2px solid black";
    sh1.el.style.height = "200px";
    sh1.el.style.width = "400px";
    root.appendChild(sh1);

    const v1 = new tgv.view.View("div");
    v1.el.style.backgroundColor = "blue";
    v1.el.style.height = "100px";
    v1.el.style.width = "100%";

    const v2 = new tgv.view.View("div");
    v2.el.style.backgroundColor = "green";
    v2.el.style.height = "100px";
    v2.el.style.width = "100%";

    const sv1 = new tgv.splitter.Splitter(tgv.splitter.Vertical, v1, v2, {sepSize:4});
    sv1.setSplit(0.9);
    sv1.el.style.marginTop = "50px";
    sv1.el.style.marginLeft = "50px";
    sv1.el.style.border = "2px solid black";
    sv1.el.style.height = "400px";
    sv1.el.style.width = "200px";
    root.appendChild(sv1);

}

tests.All.set("splitter", run);

