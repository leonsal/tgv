import * as tests from "./tests.js";
import * as tgv from "../lib/index.js";


function run() {

    // Root container
    const root = <HTMLElement>document.getElementById("root");
    const l1 = new tgv.flex.Flex("row");
    l1.setAlignItems("center");
    l1.setJustifyContent("space-between");
    l1.el.style.padding = "4px";
    root.appendChild(l1.el);

    // Buttons
    const b1 = new tgv.button.Button("Button1");
    const b1Click = (ev: Event) => console.log("button1:", ev.type);
    b1.listen(tgv.view.Click, b1Click);
    l1.appendChild(b1);

    const b2 = new tgv.button.Button("Button2", "face");
    b2.listen(tgv.view.Click, (ev) => console.log("button2:", ev.type));
    l1.appendChild(b2);

    const b3 = new tgv.button.Button("Button3", "face", tgv.button.Type.IconRight);
    b3.listen(tgv.view.Click, (ev) => console.log("button3:", ev.type));
    l1.appendChild(b3);

    const b4 = new tgv.button.Button("Button4", "alarm", tgv.button.Type.IconTop);
    b4.listen(tgv.view.Click, (ev) => {
        b1.unlisten(tgv.view.Click, b1Click);
        console.log("button1 unsubscribed");
    });
    l1.appendChild(b4);

    const b5 = new tgv.button.Button("Button5", "alarm", tgv.button.Type.IconBottom);
    b5.listen(tgv.view.Click, (ev) => {
        b1.listen(tgv.view.Click, b1Click);
        console.log("button1 subscribed again");
    });
    l1.appendChild(b5);

    const b6 = new tgv.button.Button("", "alarm");
    l1.appendChild(b6);


    // Checkboxes
    const l2 = new tgv.flex.Flex("row");
    l2.setJustifyContent("space-between");
    l2.el.style.padding = "4px";
    root.appendChild(l2.el);

    const c1 = new tgv.checkbox.Checkbox("check1");
    c1.listen(tgv.view.Click, (ev) => console.log("check1:", c1.value()));
    l2.appendChild(c1);

    const c2 = new tgv.checkbox.Checkbox("check2");
    c2.listen(tgv.view.Click, (ev) => console.log("check2:", c2.value()));
    l2.appendChild(c2);

    const c3 = new tgv.checkbox.Checkbox("check3");
    c3.listen(tgv.view.Click, (ev) => console.log("check3:", c3.value()));
    l2.appendChild(c3);

    // Radio buttons group a
    const l3 = new tgv.flex.Flex("row");
    l3.setJustifyContent("space-between");
    l3.el.style.padding = "4px";
    root.appendChild(l3.el);

    const rga = new tgv.radio.Group();
    rga.listen(tgv.radio.OnSelect, (ev)=> {
        const op = rga.selected();
        if (op) {
            console.log("group a:", op.text());
        }
    });
    const ra1 = new tgv.radio.Radio("radio a1", rga);
    ra1.listen(tgv.view.Click, (ev) => console.log("radio a1:", ra1.value()));
    l3.appendChild(ra1);

    const ra2 = new tgv.radio.Radio("radio a2", rga);
    ra2.listen(tgv.view.Click, (ev) => console.log("radio a2:", ra2.value()));
    l3.appendChild(ra2);

    const ra3 = new tgv.radio.Radio("radio a3", rga);
    ra3.listen(tgv.view.Click, (ev) => console.log("radio a3:", ra3.value()));
    l3.appendChild(ra3);

    // Radio buttons group b
    const l4 = new tgv.flex.Flex("row");
    l4.setJustifyContent("space-between");
    l4.el.style.padding = "4px";
    root.appendChild(l4.el);

    const rgb = new tgv.radio.Group();
    rgb.listen(tgv.radio.OnSelect, (ev)=> {
        const op = rgb.selected();
        if (op) {
            console.log("group b:", op.text());
        }
    });
    const rb1 = new tgv.radio.Radio("radio b1", rgb);
    rb1.listen(tgv.view.Click, (ev) => console.log("radio b1:", rb1.value()));
    l4.appendChild(rb1);

    const rb2 = new tgv.radio.Radio("radio b2", rgb);
    rb2.listen(tgv.view.Click, (ev) => console.log("radio b2:", rb2.value()));
    l4.appendChild(rb2);

    const rb3 = new tgv.radio.Radio("radio b3", rgb);
    rb3.listen(tgv.view.Click, (ev) => console.log("radio b3:", rb3.value()));
    l4.appendChild(rb3);
}

tests.All.set("button", run);
