import * as tests from "./tests.js";
import * as tgv from "../lib/index.js";

const text1 = `
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
In nec lacus vitae lacus pulvinar consequat vel eget lorem.
Vestibulum vitae egestas nunc.
Fusce eget massa sit amet nisi elementum auctor.
`
function run() {

    // Creates and appends root container to document.
    const root = new tgv.view.View("div");
    document.getElementsByTagName("body")[0].appendChild(root.el);

    const b1 = new tgv.button.Button("Info");
    b1.listen(tgv.view.Click, ()=>{
        const options: tgv.msgbox.Options = {
            title: "Info2",
            resize: "both",
            buttons: [{label: "OK2", id: "ok", icon: "face"}],
        }
        tgv.msgbox.info(text1, options, (id:string)=>console.log("Info button:", id));
    });
    root.appendChild(b1);

    const b2 = new tgv.button.Button("Warning");
    b2.listen(tgv.view.Click, ()=>{
        tgv.msgbox.warning(text1, null, (id:string)=>console.log("Warning button:", id));
    });
    root.appendChild(b2);

    const b3 = new tgv.button.Button("Confirm");
    b3.listen(tgv.view.Click, ()=>{
        tgv.msgbox.confirm(text1, null, (id:string)=>console.log("Confirm button:", id));
    });
    root.appendChild(b3);

    const b4 = new tgv.button.Button("Custom");
    b4.listen(tgv.view.Click, ()=>{
        const mb = tgv.msgbox.custom({
            title: "Custom",
            msg: text1,
            movable: true,
            resize: "both",
            icon: "face",
            buttons: [
                { label: "B1", id: "1" },
                { label: "B2", id: "2" },
                { label: "B3", id: "3" },
                { label: "Close", id: "close" },
            ],
            close: false,
        }, (id:string) => {
            console.log("Custom button:", id);
            if (id == "close") {
                mb.close();
            }
        });
    });
    root.appendChild(b4);
}

tests.All.set("msgbox", run);
