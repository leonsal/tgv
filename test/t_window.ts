import * as tests from "./tests.js";
import * as tgv from "../lib/index.js";

function run() {

    // Root container
    const root = new tgv.view.View("div");
    document.getElementsByTagName("body")[0].appendChild(root.el);

    const w1 = new tgv.window.Window({});
    w1.el.style.width = "200px";
    w1.el.style.height = "200px";
    w1.el.style.backgroundColor = "lightgray";
    w1.setContent(new tgv.button.Button("button1"));
    w1.setResize("horizontal");
    w1.setMovable(true); // no effect
    root.appendChild(w1);

    const w2 = new tgv.window.Window({title: "Window 2", content: new tgv.view.View("div")});
    w2.el.style.width = "200px";
    w2.el.style.height = "200px";
    w2.el.style.backgroundColor = "yellow";
    w2.setPos(100, 100);
    w2.setMovable(true);
    w2.setResize("vertical");
    const w2i1 = w2.appendHeaderIcon("clear");
    (<tgv.button.Button>w2i1).listen(tgv.view.Click, () => console.log("close"));
    w2.appendHeaderIcon("face");
    root.appendChild(w2);

    const w3 = new tgv.window.Window({title: "Window 3", content: new tgv.view.View("div")});
    w3.el.style.width = "300px";
    w3.el.style.height = "200px";
    w3.el.style.backgroundColor = "cyan";
    w3.setPos(200, 200);
    w3.setMovable(true);
    w3.setResize("both");
    root.appendChild(w3);

}

tests.All.set("window", run);
